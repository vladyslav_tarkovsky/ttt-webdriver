﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ttt_webdriver.PageObjects
{
    class AllSiteContent
    {
        private readonly IWebDriver _driver;

        public AllSiteContent(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        #region Controls Initializaton

        [FindsBy(How = How.LinkText, Using = "Approvals")]
        public IWebElement ApprovalsLink;

        [FindsBy(How = How.CssSelector, Using = "#viewlist17585")]
        public IWebElement ProjectsLink;

        [FindsBy(How = How.CssSelector, Using = "#viewlist17586")]
        public IWebElement TasksLink;

        [FindsBy(How = How.CssSelector, Using = "#viewlist17591")]
        public IWebElement TimesheetsLink;

        [FindsBy(How = How.CssSelector, Using = ".s4-ctx.s4-ctx-show>a>img")]
        public IWebElement PersonMenu;

        [FindsBy(How = How.CssSelector, Using = "#diidSortplts_Person")]
        public IWebElement Person;

        [FindsBy(How = How.CssSelector, Using = ".ms-paging")]
        public IWebElement TasksOnPage;

        [FindsBy(How = How.CssSelector, Using = "#bottomPagingCellWPQ1>table>tbody>tr>td>a>img")]
        public IWebElement NextPageWithTasks;

        // .ms-paging
        // #bottomPagingCellWPQ1>table>tbody>tr>td>a>img
        // .ms-imnSpan>a
        #endregion

        public IList<IWebElement> UsersInMenu // 
        {
            get
            {
                return _driver.FindElements(By.CssSelector(".ms-menuuilabelcompact>span"));
            }
        }

        public IList<IWebElement> UpprovedTasks // 
        {
            get
            {
                return _driver.FindElements(By.CssSelector(".ms-imnSpan>a"));
            }
        }
    }
}
