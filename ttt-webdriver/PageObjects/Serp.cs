﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ttt_webdriver.PageObjects
{
    public class Serp
    {
        private readonly IWebDriver _driver;

        [FindsBy(How = How.ClassName, Using = "srch-Summary")]
        private IWebElement searchSummary;

        [FindsBy(How = How.ClassName, Using = "srch-results")]
        private IWebElement searchResults;

        public Serp(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }
    }
}