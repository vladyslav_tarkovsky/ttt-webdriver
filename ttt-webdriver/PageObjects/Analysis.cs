﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;


namespace ttt_webdriver.PageObjects
{
    class Analysis
    {
        private readonly IWebDriver _driver;


        [FindsBy(How = How.LinkText, Using = "Analysis")]
        private IWebElement AnalysisMenu;

        [FindsBy(How = How.CssSelector, Using = "input[title='View']")]
        private IWebElement ViewButton;

        [FindsBy(How = How.CssSelector, Using = "select[id$='WeekMonth']")]
        public SelectElement Analysis01Drop;

        [FindsBy(How = How.CssSelector, Using = "select[id$='Week']")]
        public SelectElement Analysis02Drop;

        [FindsBy(How = How.CssSelector, Using = "nobr>span:nth-of-type(1)")] //
        public IWebElement SelectIdent;

        [FindsBy(How = How.CssSelector, Using = "input[id$='StartDateDate']")] //
        public IWebElement InputStartDate;

        [FindsBy(How = How.CssSelector, Using = "#idSearchString")] //
        public IWebElement SearchString;
        
        public Analysis(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        //private SelectElement DropList(IWebElement item) // Auxiliary - a collection of droplist elements
        //{
        //    SelectElement select = new SelectElement(item);
        //    return select;
        //}

        private IList<IWebElement> OptionCollection(SelectElement DropList) // Auxiliary - option element
        {
            IList<IWebElement> options = DropList.Options;
            return options;
        }

        //public void SelectTaskAssert() // Enumerates the values ​​of INPUT (2), for each value of INPUT (1)
        //{
        //    AnalysisMenu.Click();

        //    //SelectElement selectTime = new SelectElement(Analysis01Drop);
        //    //IList<IWebElement> options = selectTime.Options;

        //    foreach (IWebElement option in OptionCollection(DropList(Analysis01Drop)))
        //    {
        //        //var sd = option.Text;
        //        DropList(Analysis01Drop).SelectByText(option.Text);
        //        //SearchString.SendKeys(Keys.Control + "R");

        //        //SelectElement selectProject = new SelectElement(Analysis02Drop);
        //        //IList<IWebElement> options2 = selectProject.Options;
                
        //        foreach (IWebElement option2 in OptionCollection(DropList(Analysis02Drop)))
        //        {
        //            //var fg = option2.Text;
        //            DropList(Analysis02Drop).SelectByText(option2.Text);
        //        }
        //    }
        //}

        private int Rnd(int eCount) 
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }

        public void RndSelect()
        {
            AnalysisMenu.Click();
            var dr01c = OptionCollection(Analysis01Drop).Count;
            var dr02c = OptionCollection(Analysis02Drop).Count;
            var dr2indx = Rnd(dr02c);
            Analysis01Drop.SelectByIndex(0);
            Analysis01Drop.SelectByIndex(Rnd(dr01c));
            Thread.Sleep(3000);
            Analysis02Drop.SelectByIndex(0);
            Analysis02Drop.SelectByIndex(Rnd(dr02c));
            Thread.Sleep(2000);
            ViewButton.Click();
            Assert.AreEqual(SelectIdent.Text, Analysis02Drop.SelectedOption.Text);

            DateTime curDate = DateTime.Today.Date;
            var curDW = (int)curDate.DayOfWeek;
            string[] words = curDate.ToString().Split(new char[] { '.', ' ' });
            var monData = curDate.AddDays(-(curDW) + 1).Day;
            var sunData = curDate.AddDays(-(curDW) + 7).Day;
            //string assDate = words[1] + '/' + words[0] + '/' + words[2];
            string monDate = words[1] + '/' + monData + '/' + words[2];
            string sunDate = words[1] + '/' + sunData + '/' + words[2];

            var fgh = InputStartDate.GetAttribute("value");

            Thread.Sleep(1000);
            Assert.AreEqual(monDate, fgh);
        }

        
        [FindsBy(How = How.CssSelector, Using = "a[id$='Person_browse']")]
        private IWebElement BrowseButton;

        [FindsBy(How = How.CssSelector, Using = "input[id$='queryTextBox']")]
        private IWebElement InputPersonFindField;

        [FindsBy(How = How.CssSelector, Using = "input[id$='queryButton']")]
        private IWebElement InputQueryButton;

        [FindsBy(How = How.CssSelector, Using = "#ctl00_PlaceHolderDialogButtonSection_btnOk")]
        private IWebElement OkQueryButton;

        [FindsBy(How = How.CssSelector, Using = "div[id$='Person_upLevelDiv']")]
        private IWebElement InputPersonField;
        //

        public void PersonBrowseName() // does not work ????
        {
            AnalysisMenu.Click();
            ViewButton.Click();
            BrowseButton.Click();
            _driver.SwitchTo().Window(_driver.CurrentWindowHandle); // _driver.SwitchTo().Window(_driver.WindowHandles[1]);
            InputPersonFindField.SendKeys("test");
            InputQueryButton.Click();
            // 
            IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector(".ms-pb"));
            var collCount = TotalTItemColl.Count;
            for (int i = 0; i < collCount; i = (i + 6))
            {
                if (TotalTItemColl[i].Text == "testlab2")
                {
                    TotalTItemColl[i].Click();
                    OkQueryButton.Click();
                    break;
                }
            }
            var fgh = InputPersonField.GetAttribute("value");
            string[] name = fgh.ToString().Split(new char[] { ';' });
            Assert.AreEqual("testlab2", name[0]);
        }

        [FindsBy(How = How.CssSelector, Using = "a[id$='checkNames']")]
        private IWebElement NameChekButton;

        [FindsBy(How = How.CssSelector, Using = "span[id$='errorLabel']")]
        private IWebElement ErrorLabel;

        public string PersonChekWrongName(string wrName)
        {
            AnalysisMenu.Click();
            ViewButton.Click();
            InputPersonField.Clear();
            //string wrName = "gggg";
            InputPersonField.SendKeys(wrName);
            NameChekButton.Click();
            //string errText = "No exact match was found for " + wrName + ".";
            Thread.Sleep(1000);
            string yu = ErrorLabel.Text;
            //Assert.AreEqual(errText, yu);
            return yu;
        }

        public string PersonChekValidName(string Name)
        {
            AnalysisMenu.Click();
            ViewButton.Click();
            InputPersonField.Clear();
            //string wrName = "testlab2";
            InputPersonField.SendKeys(Name);
            NameChekButton.Click();
            Thread.Sleep(1000);
            string yu = InputPersonField.Text;
            //Assert.AreEqual(wrName, yu);
            return yu;
        }
    }
}
