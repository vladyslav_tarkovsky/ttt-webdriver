﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ttt_webdriver.PageObjects
{
    class SelectPeoplePopUp
    {
        private readonly IWebDriver _driver;

        public SelectPeoplePopUp(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

#region Controls Initializaton

        [FindsBy(How = How.CssSelector, Using = "input[id$='TextBox']")]
        public IWebElement QueryTextBox;

        [FindsBy(How = How.CssSelector, Using = "input[id$='queryButton']")]
        public IWebElement QueryButton;

        [FindsBy(How = How.CssSelector, Using = "input[value='OK']")]
        public IWebElement OkButton;

        // input[value='OK']
        
#endregion

        public IList<IWebElement> UserColl // Auxiliary - returns a collection of user and user-item
        {
            get
            {
                return _driver.FindElements(By.CssSelector(".ms-pb"));
            }
        }

    }
}
