﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace ttt_webdriver.PageObjects
{
    class AdminProjects
    {
        private readonly IWebDriver _driver;

        public AdminProjects(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Projects")] //.menu-item-text:contains("Projects") 
        public IWebElement AdminProjectMenu;

        [FindsBy(How = How.CssSelector, Using = ".ms-descriptiontext.plts_label_projecttaskview_addnew")]
        private IWebElement AddProject;

        [FindsBy(How = How.CssSelector, Using = "input.ms-input.plts_text_projecttaskedit")]
        private IWebElement NameProjectField;

        [FindsBy(How = How.CssSelector, Using = "textarea.ms-input.plts_text_projecttaskedit")]
        private IWebElement TasksField;

        [FindsBy(How = How.CssSelector, Using = "input[title='Save']")]
        private IWebElement SaveButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Cancel']")]
        private IWebElement CancelButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Delete']")]
        private IWebElement DeleteButton;


        public IList<IWebElement> ProjectNameColl // Auxiliary - returns a collection of items - ProjectName
        {
            get
            {
                return _driver.FindElements(By.XPath("//tr[*]/td[1]/span/a"));
            }
        }

        public IList<IWebElement> TasksNameColl // Auxiliary - returns a collection of items - Tasks Name
        {
            get
            {
                return _driver.FindElements(By.XPath("//table/tbody/tr/td[1]/table/tbody/tr[*]/td[2]/span"));
            }
        }

        public IList<IWebElement> ActivityColl // Auxiliary - returns a collection of items - Activity
        {
            get
            {
                return _driver.FindElements(By.XPath("//table/tbody/tr/td[1]/table/tbody/tr[*]/td[3]/span"));
            }
        }

        public void AddProject_()
        {
            AdminProjectMenu.Click();
            Assert.IsTrue(AddProject.Enabled);
            AddProject.Click();

            Assert.IsTrue(NameProjectField.Enabled);
            string prName = "Vl test project";
            NameProjectField.SendKeys(prName);
            TasksField.SendKeys("10, 123");
            Thread.Sleep(1000);
            CancelButton.Click();
            Assert.IsTrue(AddProject.Enabled);

            AddProject.Click();
            NameProjectField.SendKeys(prName);
            TasksField.SendKeys("10, 123");
            SaveButton.Click();
            Assert.IsTrue(AddProject.Enabled);

            _driver.FindElement(By.LinkText(prName)).Click();
            
            Assert.IsTrue(NameProjectField.Enabled);
            DeleteButton.Click();
            Thread.Sleep(2000);
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            //Assert.IsFalse(_driver.FindElement(By.LinkText(prName)).Enabled);
        }

        private int Rnd(int eCount) 
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }

        
        public void AddProjectSameNameText()
        {
            AdminProjectMenu.Click();
            Assert.IsTrue(AddProject.Enabled);
            var e = Rnd(ProjectNameColl.Count);
            var prName = ProjectNameColl[e].Text;

            AddProject.Click();
            NameProjectField.SendKeys(prName);
            SaveButton.Click();
            string errMess = "A Project already exists with the name '" + prName + "'";
            Assert.AreEqual(errMess, _driver.FindElement(By.CssSelector("span.pl_error.ms-input")).Text);
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='08']")]
        private IWebElement ActivityCheckBox;

        [FindsBy(How = How.CssSelector, Using = "textarea[id$='06']")]
        private IWebElement TaskListField;

                
        public void ProActivityChek()
        {
            AdminProjectMenu.Click();
            //Assert.IsTrue(AddProject.Enabled);
            var e = Rnd(ProjectNameColl.Count);

            var prName = ProjectNameColl[e].Text;
            //var ghj = ActivityColl[e].Text;
            ProjectNameColl[e].Click();
            var ActiveParam = ActivityCheckBox.Selected;
            ActivityCheckBox.Click();
            Thread.Sleep(2000);
            SaveButton.Click();

            IList<IWebElement> ProjectNameColl02 = _driver.FindElements(By.XPath("//tr[*]/td[1]/span/a"));
            IList<IWebElement> ActivityColl02 = _driver.FindElements(By.XPath("//tr[*]/td[3]/span"));
            
            var e2 = 0;
            for (int i = 0; i < ProjectNameColl02.Count; i++)

                if (ProjectNameColl02[i].Text == prName)
                {
                    e2 = i;
                    break;
                }
           
            if (ActiveParam == false)
                Assert.AreEqual("Yes", ActivityColl02[e2].Text);
            else
                Assert.AreEqual("No", ActivityColl02[e2].Text);
                
            Thread.Sleep(2000);
            _driver.FindElement(By.LinkText(prName)).Click();
            ActivityCheckBox.Click();
            Thread.Sleep(2000);
            SaveButton.Click();
        }

        public void TasksForProjectChek()
        {
            AdminProjectMenu.Click();
            //Assert.IsTrue(AddProject.Enabled);
            var e = Rnd(ProjectNameColl.Count);
            //var prName = ProjectNameColl[e].Text;
            //var ghj = ActivityColl[e].Text;
            var textTasks = TasksNameColl[e].Text;
            ProjectNameColl[e].Click();
            var TaskField = TaskListField.GetAttribute("value");
            CancelButton.Click();
            if (TasksNameColl[e].Text == "(All)")
                Assert.AreEqual("", TaskField);
            else
                Assert.AreEqual(textTasks, TaskField);
        }

        
        public void TasksClickChek()
        {
            AdminProjectMenu.Click();
            //Assert.IsTrue(AddProject.Enabled);
            var e = Rnd(ProjectNameColl.Count);
            var TasksName = TasksNameColl[e].Text;
            var textTasks = TasksNameColl[e].Text.Replace(", ", ",");
            string[] oneTaskName = textTasks.Split(new char[] {','});
            var hj = oneTaskName.Length;
            var gh = Rnd(oneTaskName.Length);
            var linkName = oneTaskName[gh];
            //string path = "a.[text='" + oneTaskName[gh] + "']";
            if (linkName != "(All)")
            {
                _driver.FindElement(By.LinkText(oneTaskName[gh])).Click();
                var ActiveParam = ActivityCheckBox.Selected;
                Assert.AreEqual(linkName, NameProjectField.GetAttribute("value"));
                //Assert.AreEqual(TasksName, TaskListField.GetAttribute("value"));
                Assert.IsTrue(ActiveParam);
            }
            if (oneTaskName[gh] == "(All)")
            {
                ProjectNameColl[e].Click();
                var TaskField = TaskListField.GetAttribute("value");
                Assert.AreEqual("", TaskField);
            }

        }

        /// <summary>
        /// return active project list 
        /// </summary>
        /// <returns></returns>
        public ArrayList ProjListAdm()
        {
            ArrayList ar = new ArrayList();
            AdminProjectMenu.Click();
            IList<IWebElement> ProjectNameColl01 = _driver.FindElements(By.XPath("//tr[*]/td[1]/span/a")); 
            IList<IWebElement> ActivityColl01 = _driver.FindElements(By.XPath("//tr[*]/td[3]/span")); 
            var e = ProjectNameColl01.Count;
            for (int i = 0; i < e; i++)
            {
                if (ActivityColl01[i].Text == "Yes")
                {
                    ar.Add(ProjectNameColl01[i].Text);
                }
            }
            return ar;
        }

        public string SelectRndActProject() // return random name Active project
        {
            AdminProjectMenu.Click();
            var e = ProjListAdm().Count;
            string actProj = (string)ProjListAdm()[Rnd(e)];
            //_driver.FindElement(By.LinkText(actProj)).Click();
            return actProj;
        }

        public string[] ListTaskForProject(string link) // 
        {
            Thread.Sleep(2000);
            _driver.FindElement(By.LinkText(link)).Click();
            Thread.Sleep(2000);
            string fgh = TaskListField.Text.Replace(", ", ",");
            string[] TaskList = fgh.Split(new char[] { ',' });
            return TaskList;
        }

    }
}
