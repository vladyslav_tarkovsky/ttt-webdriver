﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ttt_webdriver.PageObjects
{
    public class SignedOutPage
    {
        private readonly IWebDriver _driver;

        [FindsBy(How = How.Id, Using = "s4-simple-content")] //???
        private IWebElement messageHeadline;

        public string Headline
        {
            get
            {
                return messageHeadline.Text;
            }
        }

        [FindsBy(How = How.Id, Using = "s4-simple-error-content")] //???
        private IWebElement messageBody;

        public string Text
        {
            get
            {
                return messageBody.Text;
            }
        }

        public SignedOutPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }
    }
}