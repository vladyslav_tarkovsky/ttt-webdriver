﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;



namespace ttt_webdriver.PageObjects
{
    class AdminSettings
    {
        private readonly IWebDriver _driver;

        public AdminSettings(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Settings")] //.menu-item-text:contains("Projects") 
        public IWebElement AdminSettingsMenu;

        [FindsBy(How = How.CssSelector, Using = "input[title='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//fieldset/table/tbody/tr[2]/td[2]/select")]
        public IWebElement SelectHourDisplay;

        //public void SettingsAssert()
        //{
        //    AdminSettingsMenu.Click();
        //    Assert.IsTrue(_driver.FindElement(By.CssSelector("legend:contains('General')")).Enabled);
        //    Assert.IsTrue(_driver.FindElement(By.CssSelector("legend:contains('Permissions')")).Enabled);
        //    Assert.IsTrue(_driver.FindElement(By.CssSelector("legend:contains('Extensibility')")).Enabled);
        //    Assert.IsTrue(_driver.FindElement(By.CssSelector("legend:contains('Custom columns')")).Enabled);
        //}

        private SelectElement DropList(IWebElement item)
        {
            SelectElement select = new SelectElement(item);
            return select;
        }

        public IList<IWebElement> TotalTItemColl // Auxiliary - returns a collection of item started project
        {
            get
            {
                return _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            }
        }

        //[FindsBy(How = How.CssSelector, Using = "#WPQ2Table>tbody>tr>td>span")]
        //private IList<IWebElement> TotalTItemColl;

        //public char HourDisplay01()
        //{
        //    AdminSettingsMenu.Click();
        //    DropList(SelectHourDisplay).SelectByIndex(0);
        //    SaveButton.Click();
        //    var e = TotalTItemColl.Count;
        //    string time = TotalTItemColl[19].Text;
        //    return time[4];
        //}

        //public int HourDisplay02()
        //{
        //    AdminSettingsMenu.Click();
        //    DropList(SelectHourDisplay).SelectByIndex(1);
        //    SaveButton.Click();
        //    var e = TotalTItemColl.Count;
        //    string time = TotalTItemColl[19].Text;
        //    var f = time.Length;
        //    return f;
        //}

        public string HourDisplaySet()
        {
            string fgh = DropList(SelectHourDisplay).SelectedOption.Text;
            return fgh;
        }

        //The Project name "qqqq" exceeds the maximum length (3)
        [FindsBy(How = How.CssSelector, Using = "input.ms-input[name$='09']")]
        private IWebElement LengthLimitInput;

        [FindsBy(How = How.LinkText, Using = "Projects")] //.menu-item-text:contains("Projects") 
        private IWebElement AdminProjectMenu;

        [FindsBy(How = How.CssSelector, Using = ".ms-descriptiontext.plts_label_projecttaskview_addnew")]
        private IWebElement AddProject;

        [FindsBy(How = How.CssSelector, Using = "input.ms-input.plts_text_projecttaskedit")]
        private IWebElement NameProjectField;

        [FindsBy(How = How.CssSelector, Using = "input[title='Save']")]
        private IWebElement SavePrButton;

        public string LengthLimit(string LengthLimit, string NameProject)
        {
            AdminSettingsMenu.Click();
            LengthLimitInput.Clear();
            LengthLimitInput.SendKeys(LengthLimit);
            SaveButton.Click();
            AdminProjectMenu.Click();
            AddProject.Click();
            NameProjectField.SendKeys(NameProject);
            SavePrButton.Click();
            Thread.Sleep(1000);
            string errText = _driver.FindElement(By.CssSelector(".pl_error.ms-input")).Text;
            //Assert.AreEqual("The Project name \"qqqq\" exceeds the maximum length (3)", _driver.FindElement(By.CssSelector(".pl_error.ms-input")).Text);
            return errText;
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl13']")]
        private IWebElement ShowWorkingHoursChB;

        public void ShowWorkingHours()
        {
            AdminSettingsMenu.Click();
            if (ShowWorkingHoursChB.Selected != true)
            {
                ShowWorkingHoursChB.Click();
            }
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            Assert.AreEqual("Actual total", TotalTItemColl[0].Text);

            AdminSettingsMenu.Click();
            ShowWorkingHoursChB.Click();
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl02 = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            Assert.AreEqual("Total", TotalTItemColl02[0].Text);
        }

        [FindsBy(How = How.CssSelector, Using = "input.ms-input[name$='15']")]
        private IWebElement DailyWorkingHoursInput;

        public string DailyWorkingHours(string DailyWorkingHours)
        {
            AdminSettingsMenu.Click();
            DailyWorkingHoursInput.Clear();
            DailyWorkingHoursInput.SendKeys(DailyWorkingHours);
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            string[] tHD = TotalTItemColl[11].Text.Split(new char[] { ':', '.' });
            var fgh = tHD[0];
            return fgh;
            //Assert.AreEqual("10", tHD[0]);
        }

        [FindsBy(How = How.CssSelector, Using = "input.ms-input[name$='17']")]
        private IWebElement WeeklyWorkingHoursInput;

        public string WeeklyWorkingHours(string WeeklyWorkingHours)
        {
            AdminSettingsMenu.Click();
            WeeklyWorkingHoursInput.Clear();
            WeeklyWorkingHoursInput.SendKeys(WeeklyWorkingHours);
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            string[] tHD = TotalTItemColl[19].Text.Split(new char[] { ':', '.', ' ' });
            var fgh = tHD[1];
            //Assert.AreEqual("50", tHD[1]);
            return fgh;
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl19']")]
        private IWebElement ShowWeekendsChB;

        public void ShowWeekends()
        {
            AdminSettingsMenu.Click();
            if (ShowWeekendsChB.Selected != true)
            {
                ShowWeekendsChB.Click();
            }
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>th>span"));
            Assert.AreEqual("Sat", TotalTItemColl[7].Text);
            Assert.AreEqual("Sun", TotalTItemColl[8].Text);

            AdminSettingsMenu.Click();
            ShowWeekendsChB.Click();
            SaveButton.Click();
            IList<IWebElement> TotalTItemColl02 = _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>th>span"));
            Assert.AreEqual("Notes", TotalTItemColl02[7].Text);
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl21']")]
        private IWebElement ShowCopyLastWeekChB;
        
        [FindsBy(How = How.CssSelector, Using = "input[title='Next week']")]
        private IWebElement NextWeekBt;

        [FindsBy(How = How.CssSelector, Using = ".ms-descriptiontext.plts_label_timesheet_copy")]
        private IWebElement ShowCopyLastWeekButton;

        
        public bool IsElementPresent(IWebElement element)
        {
            try
            {
                if (element.Enabled)
                    return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            return false;
        }


        public void ShowCopyLastWeekBut(string condition) // 
        {
            AdminSettingsMenu.Click();
            if (condition == "true")
            {
                if (ShowCopyLastWeekChB.Selected != true)
                {
                    ShowCopyLastWeekChB.Click();
                }
            }
            if (condition == "false")
            {
                if (ShowCopyLastWeekChB.Selected == true)
                {
                    ShowCopyLastWeekChB.Click();
                }
            }
            SaveButton.Click();
            //NextWeekBt.Click();
            //Assert.IsTrue(IsElementPresent(ShowCopyLastWeekButton));
            
            //AdminSettingsMenu.Click();
            //ShowCopyLastWeekChB.Click();
            //SaveButton.Click();
            //NextWeekBt.Click();

            //IList<IWebElement> TotalTItemColl = _driver.FindElements(By.CssSelector(".ms-descriptiontext.plts_label_timesheet_copy"));
            //var hj = TotalTItemColl.Count;
            //Assert.AreEqual(0, hj);
        }

        public char TimeSetCheck()
        {
            _driver.FindElement(By.LinkText("Settings")).Click();
            var timeSet = HourDisplaySet();
            var a = ' ';
            if (timeSet == "Hours and minutes (3:45)")
            {
                a = ':';
            }
            else
            {
                a = '.';
            }
            return a;
        }

        public void TimeSetSetup(int setup) // Time setup (0 - if ":", 1 - if ".")
        {
            AdminSettingsMenu.Click();
            Thread.Sleep(2000);
            DropList(SelectHourDisplay).SelectByIndex(setup);
            SaveButton.Click();
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl38']")]
        private IWebElement DProjectTaskLists;

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl40']")]
        private IWebElement DApprovalsList;

        [FindsBy(How = How.CssSelector, Using = "input[id$='ctl42']")]
        private IWebElement DisplayTimesheetList;

        public void ListChBoxSet(int setup) // Display Lists Set (0 - if visible, 1 - if invisible)
        {
            AdminSettingsMenu.Click();
            var fgh = DProjectTaskLists.Selected;

            if (setup == 0)
            {
                if (DProjectTaskLists.Selected == false)
                {
                    DProjectTaskLists.Click();
                }
                if (DApprovalsList.Selected == false)
                {
                    DApprovalsList.Click();
                }
                if (DisplayTimesheetList.Selected == false)
                {
                    DisplayTimesheetList.Click();
                }
            }
            if (setup == 1)
            {
                if (DProjectTaskLists.Selected == true)
                {
                    DProjectTaskLists.Click();
                }
                if (DApprovalsList.Selected == true)
                {
                    DApprovalsList.Click();
                }
                if (DisplayTimesheetList.Selected == true)
                {
                    DisplayTimesheetList.Click();
                }
            }
            SaveButton.Click();
        }
    }
}
