﻿/*
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
*/

/*
namespace ttt_webdriver.PageObjects
{
    public class DashboardPage
    {

        private readonly IWebDriver _driver;

        public DashboardPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }
        
        [FindsBy(How = How.XPath, Using = "//a[@id='zz15_Menu']/span")]
        private IWebElement userName;

        [FindsBy(How = How.Id, Using = "zz15_Menu_t")]
        private IWebElement menuLink;

        [FindsBy(How = How.XPath, Using = "//span[@id='zz13_ID_Logout']/span")]
        private IWebElement menuLinkSignout;


        public string Userame
        {
            get
            {
                return userName.Text;
            }
        }
        

        public Serp SearchFor(string searchQuery)
        {
            return new Serp(_driver);
        }

        public SignedOutPage Logout()
        {
            menuLink.Click();
            menuLinkSignout.Click();
            return new SignedOutPage(_driver); // ???
        }

        //------------------------------------------------

        [FindsBy(How = How.Id, Using = "idSearchString")]
        private IWebElement searchString;

        [FindsBy(How = How.Id, Using = "onetIDGoSearch")]
        private IWebElement searchIcon;

        // Sends text in the search field.
        public void SearhDash(string searhText)
        {
            searchString.Clear();
            searchString.SendKeys(searhText);
            searchIcon.Click(); 
        }


        [FindsBy(How = How.XPath, Using = "(//select[@name=''])[1]")]
        public SelectElement projectDrop01;

        [FindsBy(How = How.XPath, Using = "(//select[@name=''])[2]")]
        public SelectElement projectDrop02;

        [FindsBy(How = How.CssSelector, Using = "input[name$='ctl04']")]
        public IWebElement startButton;

        [FindsBy(How = How.CssSelector, Using = "input[name$='ctl05']")]
        public IWebElement stopButton;

        [FindsBy(How = How.CssSelector, Using = "input[name$='ctl64']")]
        public IWebElement updateButton;

        /// <summary>
        /// A random order of the maximum value for the Count of collection  
        /// </summary>
        /// <param name="eCount"></param>
        /// <returns></returns>
        public int Rnd(int eCount) 
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }

        //private SelectElement DropList(IWebElement item)
        //{
        //    SelectElement select = new SelectElement(item);
        //    return select;
        //} 

        private IList<IWebElement> OptionCollection(SelectElement DropList)
        {
            IList<IWebElement> options = DropList.Options;
            return options;
        }

        public void AddTask()
        {
            var e = OptionCollection(projectDrop01).Count;
            var elDr01 = Rnd(e);
            projectDrop01.SelectByIndex(elDr01);

            var b = OptionCollection(projectDrop02).Count;
            var elDr02 = Rnd(b);
            projectDrop02.SelectByIndex(elDr02);

            startButton.Click();
            Thread.Sleep(60000);
            stopButton.Click();
            updateButton.Click();
        }
        
        //public void AddTask() 
        //{
        //    SelectElement select = new SelectElement(projectDrop01);
        //    IList<IWebElement> options = select.Options;
        //    var e = options.Count;

        //    Random rand = new Random();
        //    int temp;
        //    temp = rand.Next(e);
        //    select.SelectByIndex(temp);

        //    SelectElement select2 = new SelectElement(projectDrop02);
        //    IList<IWebElement> options2 = select2.Options;
        //    var b = options2.Count;

        //    int tmp;
        //    tmp = rand.Next(b);
        //    select2.SelectByIndex(tmp);

        //    startButton.Click();
        //    Thread.Sleep(1000);
        //    stopButton.Click();
        //    Thread.Sleep(1000);
        //    updateButton.Click();
        //    Thread.Sleep(1000);
        //}

        public IList<IWebElement> DelItemColl // Auxiliary - returns a collection of items - icon to delete the task
        {
            get
            {
                return _driver.FindElements(By.XPath("(//img[@alt='Remove row'])[position()>1]"));
            }
        }

                
        public void DelTask() 
        {
            var e = DelItemColl.Count;
            var path = "(//img[@alt='Remove row'])[" + (e+1) + "]"; // ???
            _driver.FindElement(By.XPath(path)).Click();
            Thread.Sleep(2000);
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            updateButton.Click();
            Thread.Sleep(2500);
        }

        //public void SelectI() 
        //{
        //    //var text = projectDrop01.GetAttribute("value") + projectDrop02.GetAttribute("value");
        //    foreach (IWebElement option in OptionCollection(DropList(projectDrop01)))
        //    {
        //        DropList(projectDrop01).SelectByText(option.Text);
        //        Thread.Sleep(1000);
                
        //        foreach (IWebElement option2 in OptionCollection(DropList(projectDrop02)))
        //        {
        //            DropList(projectDrop02).SelectByText(option2.Text);
        //            //startButton.Click();
        //            //Thread.Sleep(1000);
        //            //stopButton.Click();
        //        }
        //    }
        //}

        public IList<IWebElement> TaskItemColl // Auxiliary - returns a collection of open tasks
        {
            get
            {
                return _driver.FindElements(By.XPath("(//select[@name=''])[position()>4]"));
            }
        }

        //public void Collec() 
        //{
        //    projectDrop01.GetAttribute("value").ToString();

        //    foreach (IWebElement option in OptionCollection(DropList(projectDrop01)))
        //    {
        //        DropList(projectDrop01).SelectByText(option.Text);
        //        Thread.Sleep(1000);

        //        foreach (IWebElement option2 in OptionCollection(DropList(projectDrop02)))
        //        {
        //            DropList(projectDrop02).SelectByText(option2.Text);
        //            //startButton.Click();
        //            //Thread.Sleep(1000);
        //            //stopButton.Click();
        //        }
        //    }

        //    var e = TaskItemColl.Count;
        //    var path = "(//img[@alt='Remove row'])[" + (e / 2 - 1) + "]";
        //    _driver.FindElement(By.XPath(path)).Click();
        //    Thread.Sleep(2000);
        //    IAlert alert = _driver.SwitchTo().Alert();
        //    alert.Accept();
        //}

        public IList<IWebElement> InputItemCollMon // Auxiliary - returns a collection of fields time Mon
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[3]/input"));
            }
        }

        [FindsBy(How = How.CssSelector, Using = "span.pl_error.ms-input")]
        private IWebElement messageOvertime;

        public string AddWrongDataHours() 
        {
            var e = InputItemCollMon.Count;
            InputItemCollMon[0].Clear();
            InputItemCollMon[0].SendKeys("25");
            updateButton.Click();
            var messOvertime = messageOvertime.Text;
            return messOvertime;
            // Assert.AreEqual("It is not possible to work more than 24 hours in a day.", messageOvertime.Text);
        }


        public IList<IWebElement> ActTotalItemColl // Auxiliary - returns a collection of Actual total - Target total
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[*]/td[2]/span"));
            }
        }

        //public void TimeSumMon()
        //{
        //    var timeMon = 0;
        //    var nTaskInt = 0;
        //    foreach (IWebElement option in InputItemCollMon)
        //    {
        //        string input = option.GetAttribute("value").Split(new char[] { ':' })[0]; // 12:00
        //        nTaskInt = Convert.ToInt32(input);
        //        timeMon = timeMon + nTaskInt;
        //    }

        //    var actMon = 0;
        //    var aTaskInt = 0;
        //    string actTotal = ActTotalItemColl[0].Text.Split(new char[] { ':' })[0];
        //    aTaskInt = Convert.ToInt32(actTotal);
        //    actMon = actMon + aTaskInt;

        //    Assert.AreEqual(timeMon, actMon);
        //}

        [FindsBy(How = How.CssSelector, Using = "input[id$='Date']")] 
        public IWebElement InputDate;

        [FindsBy(How = How.CssSelector, Using = "img[id$='DatePickerImage']")] 
        public IWebElement DatePickerImage;

        [FindsBy(How = How.CssSelector, Using = "input[title='Previous week']")] 
        public IWebElement PreviousWeekButt;

        [FindsBy(How = How.CssSelector, Using = "input[title='Next week']")] 
        public IWebElement NextWeekButt;

        [FindsBy(How = How.CssSelector, Using = "img[id$='DateDatePickerImage']")] 
        public IWebElement CalButton;


        public void TimeSum() // The sum of time for each day + Total
        {
            //InputDate.Clear();
            //InputDate.SendKeys(Keys.Control + "a" + "11/5/2012");
            //Thread.Sleep(3000);
            //NextWeekButt.Click();
            //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            var allDayTotal = 0;
            var sumTime = 0;
            var modul = 60; // Factor for converting time. Depends on the settings in Administration -> Settings
            var countProject = _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[3]/input")).Count ;

            for (int i = 3; i < 10; i++)
            {
                var tameDay = 0;
                var totatTimeDay = 0;
                

                string pathDoW = "//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[" + i + "]/input";
                IList<IWebElement> ActTotalItem = _driver.FindElements(By.XPath(pathDoW));
                
                foreach (IWebElement option in ActTotalItem)
                {
                    string[] input = option.GetAttribute("value").ToString().Split(new char[] { ':', '.' });

                    if (input.Length > 1)
                        tameDay = tameDay + (Convert.ToInt32(input[0]) * modul) + (Convert.ToInt32(input[1]));
                    else
                        tameDay = tameDay + (Convert.ToInt32(input[0]) * modul);
                    Thread.Sleep(1000);
                }

                string totalHourDay = "//*[@id='WPQ2Table']/tbody/tr[" + (countProject + 4) + "]/td[" + (i - 1) + "]/span";
                Thread.Sleep(1000);
                string[] tHD = _driver.FindElement(By.XPath(totalHourDay)).Text.Split(new char[] { ':','.' });
                if (tHD.Length > 1)
                    totatTimeDay = totatTimeDay + (Convert.ToInt32(tHD[0]) * modul) + (Convert.ToInt32(tHD[1]));
                else
                    totatTimeDay = totatTimeDay + (Convert.ToInt32(tHD[0]) * modul);

                Assert.AreEqual(totatTimeDay, tameDay);
                allDayTotal = allDayTotal + totatTimeDay;
                Thread.Sleep(1000);
                }

            var AllTimeSum = _driver.FindElement(By.XPath("//*[@id='WPQ2Table']/tbody/tr[" + (countProject + 4) + "]/td[10]/span"));
            string[] sumHD = AllTimeSum.Text.Split(new char[] { ':', '.', ' ' });

            if (sumHD.Length > 2)
                sumTime = sumTime + (Convert.ToInt32(sumHD[1]) * modul) + (Convert.ToInt32(sumHD[2]));
            else
                sumTime = sumTime + (Convert.ToInt32(sumHD[1]) * modul);
            Assert.AreEqual(sumTime, allDayTotal);
        }

        public ArrayList ProjList() // list active project in Dashboard
        {
            _driver.FindElement(By.LinkText("My Dashboard")).Click();
            ArrayList arr = new ArrayList();
            IList<IWebElement> options = projectDrop01.Options;
            foreach (IWebElement option in options)
            {
                arr.Add(option.Text);
            }
            return arr;
        }

        /// <summary>
        /// 
        /// </summary>
        public ArrayList TaskList (string link)
        {
            _driver.FindElement(By.LinkText("My Dashboard")).Click();
            ArrayList arr = new ArrayList();
            //SelectElement selectPro = new SelectElement(projectDrop01);
            projectDrop01.SelectByText(link);
            IList<IWebElement> options = projectDrop02.Options;
            foreach (IWebElement option in options)
            {
                arr.Add(option.Text);
            }
            return arr;
        }



        // Stop button displayed
        // //*[@id='WPQ1Table']/tbody/tr[2]/td[*]/span

        public void StartRndTask(int dr1, int dr2) // start random task
        {
            projectDrop01.SelectByIndex(dr1);

            projectDrop02.SelectByIndex(dr2);

            startButton.Click();
        }

        public Boolean StopBottEn()
        {
            var fg = stopButton.Enabled;
            return fg;
        }


        public IList<IWebElement> StartTaskColl // Auxiliary - returns a collection of item started project
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ1Table']/tbody/tr[2]/td[*]/span"));
            }
        }

        public string FromDropProName(int Rnd) // get text from "Rnd" item from project droplist
        {
            string e = OptionCollection(projectDrop01)[Rnd].Text;
            return e;
        }

        public string FromDropTaskName(int Rnd) // get text from "Rnd" item from task droplist
        {
            string f = OptionCollection(projectDrop02)[Rnd].Text;
            return f;
        }

        public string StartProName()
        {
            string e = StartTaskColl[0].Text; 
            return e;
        }

        public string StartTaskName()
        {
            string e = StartTaskColl[1].Text;
            return e;
        }
    }
 }
*/