﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ttt_webdriver.PageObjects
{
    class TeamDash
    {
        private readonly IWebDriver _driver;

        [FindsBy(How = How.LinkText, Using = "Dashboard")]
        private IWebElement dDashboard;

        [FindsBy(How = How.LinkText, Using = "Analysis")]
        private IWebElement analysisMenu;

        public TeamDash(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        private int Rnd(int eCount) 
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }


        public IList<IWebElement> WeekItemColl // Auxiliary - returns a collection of items - Week
        {
            get
            {
                return _driver.FindElements(By.XPath("//tr[*]/td[1]/span"));
            }
        }

        public IList<IWebElement> PersonItemColl // Auxiliary - returns a collection of items - Person
        {
            get
            {
                return _driver.FindElements(By.XPath("//tr[*]/td[2]/span"));
            }
        }

       
        
        public void TeamDashAssert()
        {
            var pathButton = "input[id*='Button"; 
            dDashboard.Click();
            Assert.AreEqual("Timesheets to approve", _driver.FindElement(By.CssSelector("h3")).Text);
            var e = WeekItemColl.Count;
            var nBut = Rnd(e);
            var weekText = WeekItemColl[nBut].Text;
            var personText = PersonItemColl[nBut].Text;
            _driver.FindElement(By.CssSelector(pathButton + nBut + "']")).Click();
            Thread.Sleep(2500);
            var weekTextField = _driver.FindElement(By.CssSelector("input[id$='Date']")).GetAttribute("value");
            var ownerField = _driver.FindElement(By.XPath("//*[@id='content']")).Text;
            Assert.AreEqual(weekText, weekTextField);
            Assert.AreEqual(personText, ownerField);
        }


        public IList<IWebElement> StartProjectItemColl // Auxiliary - returns a collection of items - StartProject
        {
            get
            {
                return _driver.FindElements(By.CssSelector("[id*='row']>td>span"));
            }
        }

        [FindsBy(How = How.CssSelector, Using = "input[title='Stop']")]
        private IWebElement StopProjectButton;



    }
}
