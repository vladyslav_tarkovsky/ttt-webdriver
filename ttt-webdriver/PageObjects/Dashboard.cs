﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;



namespace ttt_webdriver.PageObjects
{
    public class DashboardPage
    {

        private readonly IWebDriver _driver;

        public DashboardPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        #region Controls Initializaton

        [FindsBy(How = How.XPath, Using = "//a[@id='zz15_Menu']/span")]
        private IWebElement userName;

        [FindsBy(How = How.Id, Using = "zz15_Menu_t")]
        private IWebElement menuLink;

        [FindsBy(How = How.XPath, Using = "//span[@id='zz13_ID_Logout']/span")]
        private IWebElement menuLinkSignout;

        [FindsBy(How = How.XPath, Using = "(//select[@name=''])[1]")]
        public IWebElement projectDrop01;

        [FindsBy(How = How.XPath, Using = "(//select[@name=''])[2]")]
        public IWebElement projectDrop02;

        [FindsBy(How = How.CssSelector, Using = "input[name$='ctl04']")]
        public IWebElement startButton;

        [FindsBy(How = How.CssSelector, Using = "input[name$='ctl05']")]
        public IWebElement stopButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Update']")]
        public IWebElement updateButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Approve']")]
        public IWebElement approveButton;


        [FindsBy(How = How.LinkText, Using = "Add row")]
        public IWebElement addRowButton;

        [FindsBy(How = How.XPath, Using = "//*[@id='idSearchString']")]
        public IWebElement searchField;

        [FindsBy(How = How.CssSelector, Using = "input[title='Previous week']")]
        public IWebElement previousWeek;

        [FindsBy(How = How.CssSelector, Using = "input[title='Next week']")]
        public IWebElement nextWeek;

        [FindsBy(How = How.CssSelector, Using = "input[name$='Date']")]
        public IWebElement dateField;

        [FindsBy(How = How.CssSelector, Using = "img[id$='DateDatePickerImage']")]
        public IWebElement DatePicker;

        [FindsBy(How = How.CssSelector, Using = "iframe[id$='DateDatePickerFrame']")]
        public IWebElement DatePickerFrame;

        [FindsBy(How = How.CssSelector, Using = ".pl_error.ms-input")]
        public IWebElement ErrorMess;

        [FindsBy(How = How.CssSelector, Using = "img[title='Choose person']")]
        public IWebElement ChoosePerson;

        [FindsBy(How = How.CssSelector, Using = "#content")]
        public IWebElement CurrentUser;

        [FindsBy(How = How.XPath, Using = "//*[@id='WPQ1Table']/tbody/tr[3]/td/span")]
        public IWebElement startErrorMessage;

        [FindsBy(How = How.CssSelector, Using = "input[title='Unapprove']")]
        public IWebElement Unapprove;

        [FindsBy(How = How.CssSelector, Using = ".ms-descriptiontext.plts_label_timesheet_copy")]
        public IWebElement CopyLastWeekLink;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'All Site Content')]")]
        public IWebElement AllSiteContent;


        // //span[contains(text(),'All Site Content')]


        #endregion

        public string Userame
        {
            get
            {
                return userName.Text;
            }
        }


        public Serp SearchFor(string searchQuery)
        {
            return new Serp(_driver);
        }

        public SignedOutPage Logout()
        {
            menuLink.Click();
            menuLinkSignout.Click();
            return new SignedOutPage(_driver); // ???
        }

        //------------------------------------------------

        [FindsBy(How = How.Id, Using = "idSearchString")]
        private IWebElement searchString;

        [FindsBy(How = How.Id, Using = "onetIDGoSearch")]
        private IWebElement searchIcon;



        // Sends text in the search field.
        public void SearhDash(string searhText)
        {
            searchString.Clear();
            searchString.SendKeys(searhText);
            searchIcon.Click();
        }

        /// <summary>
        /// A random order of the maximum value for the Count of collection  
        /// </summary>
        /// <param name="eCount"></param>
        /// <returns></returns>
        public int Rnd(int eCount)
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }

        public SelectElement DropList(IWebElement item)
        {
            SelectElement select = new SelectElement(item);
            return select;
        }

        public IList<IWebElement> OptionCollection(SelectElement DropList)
        {
            IList<IWebElement> options = DropList.Options;
            return options;
        }

        // ----------------------------------

        public ArrayList ProjList() // list active project in Dashboard
        {
            _driver.FindElement(By.LinkText("My Dashboard")).Click();
            ArrayList arr = new ArrayList();
            IList<IWebElement> options = DropList(projectDrop01).Options;
            foreach (IWebElement option in options)
            {
                arr.Add(option.Text);
            }
            return arr;
        }


        public ArrayList TaskList(string link)
        {
            _driver.FindElement(By.LinkText("My Dashboard")).Click();
            ArrayList arr = new ArrayList();
            //SelectElement selectPro = new SelectElement(projectDrop01);
            DropList(projectDrop01).SelectByText(link);
            IList<IWebElement> options = DropList(projectDrop02).Options;
            foreach (IWebElement option in options)
            {
                arr.Add(option.Text);
            }
            return arr;
        }

        public void StartRndTask(int dr1, int dr2) // start random task
        {
            DropList(projectDrop01).SelectByIndex(dr1);
            DropList(projectDrop02).SelectByIndex(dr2);

            startButton.Click();
        }

        public Boolean StopBottEn()
        {
            var fg = stopButton.Enabled;
            return fg;
        }


        public IList<IWebElement> StartTaskColl // Auxiliary - returns a collection of item started project
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ1Table']/tbody/tr[2]/td[*]/span"));
            }
        }



        public string FromDropProName(int Rnd) // get text from "Rnd" item from project droplist
        {
            return OptionCollection(DropList(projectDrop01))[Rnd].Text;
        }

        public string FromDropTaskName(int Rnd) // get text from "Rnd" item from task droplist
        {
            return OptionCollection(DropList(projectDrop02))[Rnd].Text;
        }


        public IList<IWebElement> OpenProject // Auxiliary - returns a collection of open project
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[1]/select"));
            }
        }

        public IList<IWebElement> OpenTasks // Auxiliary - returns a collection of open tasks
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[2]/select"));
            }
        }

        public IList<IWebElement> OpenTaskCells // Auxiliary - returns a collection of open tasks cells
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[2]/select"));
            }
        }


        public int NewPrTaskPresent(string prName, string taskName) // all right
        {
            var e = OpenProject.Count;
            var hj = 0;
            for (int i = 0; i < e; )
            {
                var b = DropList((OpenProject)[i]).SelectedOption.Text;
                if (b == prName)
                {
                    var f = DropList((OpenTasks)[i]).SelectedOption.Text;
                    if (f == taskName)
                    {
                        hj = i;
                    }
                }
                i++;
            }
            return hj;
        }

        public int GetTimeFromeCell(string path, char a, int mod)
        {
            var TmSpent = _driver.FindElement(By.XPath(path)).GetAttribute("value");
            string[] input = TmSpent.Split(new char[] { a });
            var TimeSpent = (Convert.ToInt32(input[0]) * mod) + Convert.ToInt32(input[1]);
            return TimeSpent;
        }

        public IList<IWebElement> RemoveRowImg // Auxiliary - returns a collection of Remove Img
        {
            get
            {
                return _driver.FindElements(By.CssSelector("img[title='Remove row']"));
            }
        }

        public IList<IWebElement> ProTimeCells // Auxiliary - returns a collection of first project time cells 
        {
            get
            {
                return _driver.FindElements(By.CssSelector(".ms-input.plts_text_timesheet.plts_text_timesheet_hour"));
            }
        }

        // #WPQ2Table>tbody>tr>td>span

        public IList<IWebElement> TotalTimeSum // Auxiliary - returns a collection sum time per day and Total time
        {
            get
            {
                return _driver.FindElements(By.CssSelector("#WPQ2Table>tbody>tr>td>span"));
            }
        }

        //  //*[@id='WPQ2Table']/tbody/tr[position()>2]/td[11]/img

        public IList<IWebElement> DelRowColl // Auxiliary - returns a collection delete row img
        {
            get
            {
                return _driver.FindElements(By.XPath("//*[@id='WPQ2Table']/tbody/tr[position()>2]/td[13]/img"));
            }
        }

        // img[alt='Add row']

        public IList<IWebElement> AddCopyButtColl // Auxiliary - returns a collection delete row img
        {
            get
            {
                return _driver.FindElements(By.CssSelector("img[alt='Add row']"));
            }
        }

        public int TimeTextToInt(string b, int mod)
        {
            string[] input = b.Split(new char[] { ':', '.', ' ' });
            if (input[0] == "=")
            {
                var TimeSpentDay = (Convert.ToInt32(input[1]) * mod) + Convert.ToInt32(input[2]);
                return TimeSpentDay;
            }
            else
            {
                var TimeSpentDay = (Convert.ToInt32(input[0]) * mod) + Convert.ToInt32(input[1]);
                return TimeSpentDay;
            }
        }

        public string DateString(DateTime dateSet)
        {
            string[] dateSetString = dateSet.ToString().Split(new char[] { '.', ' ', ':' });

            //var day = int.Parse(dateSetString[0]);
            var day = Convert.ToInt32(dateSetString[0]);
            //var month = int.Parse(dateSetString[1]);
            var month = Convert.ToInt32(dateSetString[1]);

            string curDateString = month + "/" + day + "/" + dateSetString[2];
            return curDateString;
        }


        public ArrayList DateListInCell(IList<IWebElement> coll)
        {
            ArrayList myList = new ArrayList();
            foreach (IWebElement option in coll)
            {
                myList.Add(option.GetAttribute("value"));
            }
            return myList;
        }

        public void AddRowRndProjectAndTask()
        {
            addRowButton.Click();

            var OpenProjectCount = OpenProject.Count;
            var OpenTasksCount = OpenTasks.Count;

            var drop1 = Rnd(DropList(OpenProject[OpenProjectCount - 1]).Options.Count);
            DropList(OpenProject[OpenProjectCount - 1]).SelectByIndex(drop1);

            var drop2 = Rnd(DropList(OpenTasks[OpenTasksCount - 1]).Options.Count);
            DropList(OpenTasks[OpenTasksCount - 1]).SelectByIndex(drop2);
        }

        public void DelAllRows()
        {
            var delCount = DelRowColl.Count;
            for (int i = 0; i < delCount; i++)
            {
                DelRowColl[0].Click();
                IAlert alert = _driver.SwitchTo().Alert();
                alert.Accept();
            }
            updateButton.Click();
        }

        public void AddManyTasks(int taskCount)
        {
            var delCount = DelRowColl.Count;
            DelRowColl[delCount - 1].Click();
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            var a = 0;
            var b = 0;
            for (int i = 0; i < taskCount; i++)
            {
                try
                {
                    addRowButton.Click();
                    DropList(OpenProject[i]).SelectByIndex(a);
                    DropList(OpenTasks[i]).SelectByIndex(b);
                    b++;
                }
                catch (NoSuchElementException)
                {
                    b = 0;
                    a++;
                    DropList(OpenProject[i]).SelectByIndex(a);
                    DropList(OpenTasks[i]).SelectByIndex(b);
                    b++;
                }
                Thread.Sleep(1000);
                ProTimeCells[(i * 7) + 7].Clear();
                ProTimeCells[(i * 7) + 7].SendKeys("0:01");
            }
        }
    }
}