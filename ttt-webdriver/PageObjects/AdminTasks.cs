﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace ttt_webdriver.PageObjects
{
    class AdminTasks
    {
        private readonly IWebDriver _driver;

        public AdminTasks(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Tasks")] //.menu-item-text:contains("Projects") 
        private IWebElement AdminTasksMenu;

        [FindsBy(How = How.CssSelector, Using = ".ms-descriptiontext.plts_label_projecttaskview_addnew")]
        private IWebElement AddTasks;

        [FindsBy(How = How.CssSelector, Using = "input.ms-input.plts_text_projecttaskedit")]
        private IWebElement NameTasksField;

        [FindsBy(How = How.CssSelector, Using = "textarea.ms-input.plts_text_projecttaskedit")]
        private IWebElement ProjectField;

        [FindsBy(How = How.CssSelector, Using = "input[title='Cancel']")]
        private IWebElement CancelButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Save']")]
        private IWebElement SaveButton;

        [FindsBy(How = How.CssSelector, Using = "input[title='Delete']")]
        private IWebElement DeleteButton;

        public void AddTasks_()
        {
            AdminTasksMenu.Click();
            Assert.IsTrue(AddTasks.Enabled);
            AddTasks.Click();

            Assert.IsTrue(NameTasksField.Enabled);
            string prName = "Vl test task";
            NameTasksField.SendKeys(prName);
            ProjectField.SendKeys("Test1, Test2");
            Thread.Sleep(1000);
            CancelButton.Click();
            Assert.IsTrue(AddTasks.Enabled);

            AddTasks.Click();
            NameTasksField.SendKeys(prName);
            ProjectField.SendKeys("10, 123");
            SaveButton.Click();
            Assert.IsTrue(AddTasks.Enabled);

            _driver.FindElement(By.LinkText(prName)).Click();

            Assert.IsTrue(NameTasksField.Enabled);
            DeleteButton.Click();
            Thread.Sleep(2000);
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
        }

        private int Rnd(int eCount) // Рандомный порядок элемента по максимальному значению Count для коллекции
        {
            Random rand = new Random();
            int temp = rand.Next(eCount);
            return temp;
        }

        public IList<IWebElement> TasksNameColl // 
        {
            get
            {
                return _driver.FindElements(By.XPath("//tr[*]/td[1]/span/a"));
            }
        }

        public IList<IWebElement> ActivityColl //
        {
            get
            {
                return _driver.FindElements(By.XPath("//tr[*]/td[3]/span"));
            }
        }

        public void AddTaskSameNameText()
        {
            AdminTasksMenu.Click();
            Assert.IsTrue(AddTasks.Enabled);
            var e = Rnd(TasksNameColl.Count);
            var prName = TasksNameColl[e].Text;

            AddTasks.Click();
            NameTasksField.SendKeys(prName);
            SaveButton.Click();
            string errMess = "A Task already exists with the name '" + prName + "'";
            Assert.AreEqual(errMess, _driver.FindElement(By.CssSelector("span.pl_error.ms-input")).Text);
        }

        [FindsBy(How = How.CssSelector, Using = "input[id$='08']")]
        private IWebElement ActivityCheckBox;

        [FindsBy(How = How.CssSelector, Using = "textarea[id$='06']")]
        private IWebElement TaskListField;



        public ArrayList ActiveTaskList() // return list active tasks (Test - ProjListCheck)
        {
            AdminTasksMenu.Click();
            ArrayList ar = new ArrayList();
            var e = TasksNameColl.Count;
            for (int i = 0; i < e; i++)
            {
                if (ActivityColl[i].Text == "Yes")
                {
                    ar.Add(TasksNameColl[i].Text);
                }
            }
            return ar;
        }

        //public string[] NameRndActProject()
        //{
        //    AdminTasksMenu.Click();
        //    var e = ActiveTaskList().Count;
        //    string actProj = (string)ActiveTaskList()[Rnd(e)];
        //    _driver.FindElement(By.LinkText(actProj)).Click();
        //    string fgh = TaskListField.Text.Replace(", ", ",");
        //    string[] TaskList = fgh.Split(new char[] { ',' });
        //    return TaskList;
        //}

        /// <summary>
        /// return list active task for all  projects
        /// 
        /// </summary>
        /// <returns></returns>
        public ArrayList ActivTaskForAll()
        {
            AdminTasksMenu.Click();
            ArrayList ar = new ArrayList();
            var e = ActiveTaskList().Count;

            IList<IWebElement> TaskNameColl01 = _driver.FindElements(By.XPath("//tr[*]/td[1]/span/a"));
            IList<IWebElement> ActivityForAll = _driver.FindElements(By.XPath("//tr/td[1]/table/tbody/tr[*]/td[2]/span"));
            for (int i = 0; i < e; i++)
            {
                if (ActivityForAll[i].Text == "(All)")
                {
                    ar.Add(TaskNameColl01[i].Text);
                }
            }
            return ar;
        }
       

    }

}
