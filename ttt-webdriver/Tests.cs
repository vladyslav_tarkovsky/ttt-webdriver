﻿
using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using ttt_webdriver.PageObjects;
using System.Diagnostics;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;

namespace ttt_webdriver
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver _driver;
        private WebDriverWait _wait;
        private string _protocol = @"http://";
        private string _baseUrl = @"spf2010.qa.pentalogic.net:2010/TeamTime/default.aspx";
        private readonly string[,] _fullCredentials = new[,] { { "TestLab", "Xeph7nAw" } };
        //Demo purposes: defaults to firefox, specify or ie, chrome. Later, this option can externalized
        private string Browser = "chrome"; // firefox
        private TraceSwitch _loglevel = new TraceSwitch("General", "Entire Application");
        private string proxyaddr = "localhost";
        private int proxyport = 8080;
        public string _targetUrl = string.Empty;

        
        [SetUp]
        public void Setup()
        {
            switch (Browser)
            {
                case "ie":
                    _driver = new InternetExplorerDriver();
                    _targetUrl = string.Format("{0}{1}", _protocol, _baseUrl);
                    break;
                case "chrome":
                    var options = new ChromeOptions();
                    //Overriding the default browser location
                    //options.BinaryLocation = "C:\\GoogleChromePortable\\GoogleChromePortable.exe";
                    options.AddArgument("start-maximized");
                    _driver = new ChromeDriver(options);
                    _targetUrl = string.Format("{0}{1}:{2}@{3}", _protocol, _fullCredentials[0, 0], _fullCredentials[0, 1], _baseUrl);
                    break;
                default:
                    var profile = new FirefoxProfile("testprofile\\"); // 
                    _driver = new FirefoxDriver(profile);
                    _targetUrl = string.Format("{0}{1}:{2}@{3}", _protocol, _fullCredentials[0, 0], _fullCredentials[0, 1], _baseUrl);
                    break;
            }
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(20));
            _driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void Teardown()
        {
            WinHelper.SendEsc();
            _driver.Close();
            _driver.Quit();
        }

        private void IElogin()
        {
            if (Browser == "ie")
            {
                WinHelper.ProcessIeNtlmAuth("pentalogic.net\\" + _fullCredentials[0, 0], _fullCredentials[0, 1]);
                _wait.Until(d => d.FindElement(By.Id("zz15_Menu_t")).Enabled);
            }
        }

        private void IEWait(int period)
        {
            if (Browser == "ie")
            {
                Thread.Sleep(period);
            }
        }

        [Test, Description("HTTP Digest auth"), Category("Release")]
        public void _00_Authorize()
        {
            //_driver.Manage().Cookies.AddCookie(new Cookie("WSS_KeepSessionAuthenticated", "{ab8c2c7a-a655-4557-a65c-807a5e3f2a62}", "spf2010.qa.pentalogic.net", "/", new DateTime(2013, 12, 29)));
            //_driver.
            _driver.Navigate().GoToUrl(_targetUrl);
            IElogin();
            var dashboard = new DashboardPage(_driver);
            string acName = @"SPF2010\" + (string)_fullCredentials[0, 0].ToLower();
            Assert.AreEqual(dashboard.Userame, acName, "Invalid Username");
            //Assert.AreEqual(dashboard.Userame, _fullCredentials[0, 0]);
            //Assert.True(dashboard.Userame.Equals(_fullCredentials[0, 0]), "Login with {0}:{1} credentials failed", _fullCredentials[0, 0], _fullCredentials[0, 1]);
            var signedout = dashboard.Logout();
            if (Browser == "ie")
                WinHelper.CancelPopup();
            Assert.True(signedout.Text.Equals("You must close your browser to complete the sign out process."), "Logout wasn't successful");
            //Sample of logging events for any reason other than Assertion
            Trace.Write("Event");
            if (_loglevel.TraceVerbose)
                Trace.Write("verbose event");
            }

        /// <summary>
        /// Check options in Project dropdown. Options should match those in Projects view.
        /// </summary>
        [Test, Description("ProjListCheck"), Category("Release")]
        public void _01_ProjListCheck()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var admin = new AdminProjects(_driver);
            var dash = new DashboardPage(_driver);
            Assert.AreEqual(admin.ProjListAdm(), dash.ProjList(), "Projects view options list doesn't match the dropdown contents");
        }

        /// <summary>
        /// As Project is changed, the Tasks drop down should show only the associated Tasks (as shown in the Projects view)
        /// </summary>
        [Test, Description("TasksListCheck"), Category("Release")]
        public void _02_TasksListCheck()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var adminPr = new AdminProjects(_driver);
            var adminTask = new AdminTasks(_driver);
            var dash = new DashboardPage(_driver);

            var ProjListAdm = adminPr.ProjListAdm();
            var ActiveTaskForAll = adminTask.ActivTaskForAll();
            var ActiveTaskList = adminTask.ActiveTaskList();
            var e = ProjListAdm.Count;
            adminPr.AdminProjectMenu.Click();

            for (int i = 0; i < e; i++)
            {
                var ProjectNameColl = adminPr.ProjectNameColl;
                var TaskColl = adminPr.TasksNameColl;
                string addTaskLst = TaskColl[i].Text.Replace(", ", ",");
                string[] addTaskList = addTaskLst.Split(new char[] { ',' });
                var dashTaskList = dash.TaskList(ProjectNameColl[i].Text);
                var myArrayList = new ArrayList(addTaskList);
                if (addTaskLst != "(All)")
                {
                    var TaskList = new ArrayList();
                    TaskList.AddRange(myArrayList);
                    TaskList.AddRange(ActiveTaskForAll);
                    Assert.AreEqual(TaskList, dashTaskList);
                }
                else
                {
                    ActiveTaskList.Sort();
                    dashTaskList.Sort();
                    Assert.AreEqual(ActiveTaskList, dashTaskList);
                }
                adminPr.AdminProjectMenu.Click();
            }
        }

        /// <summary>
        /// Choose project/task and click Start button
        /// Dropdowns become text
        /// 'Hours' column shows time since click, in current 'Hour Display' format chosen on Settings page
        /// On the 'Team Overview' page a matching entry should be shown on the Status web part
        /// </summary>
        [Test, Description("ClickStartButton"), Category("Release")]
        public void _03_ClickStartButton()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var team = new TeamDash(_driver);
            var set = new AdminSettings(_driver);
            
            var drop1 = dash.Rnd(dash.DropList(dash.projectDrop01).Options.Count);
            var prName = dash.FromDropProName(drop1);
            dash.DropList(dash.projectDrop01).SelectByIndex(drop1);
            IEWait(4000);
            var drop2 = dash.Rnd(dash.DropList(dash.projectDrop02).Options.Count);
            var taskName = dash.FromDropTaskName(drop2);
            dash.DropList(dash.projectDrop02).SelectByIndex(drop2);
            
            dash.startButton.Click();

            var StartProName = dash.StartTaskColl[0].Text;
            var StartTaskName = dash.StartTaskColl[1].Text;

            Assert.IsTrue(dash.StopBottEn(), "Stop button is absent");
            Assert.AreEqual(StartProName, prName, "Projects display name does not match the name of the selected Projects.");
            Assert.AreEqual(StartTaskName, taskName, "Task display name does not match the name of the selected Task.");
            _driver.FindElement(By.LinkText("Dashboard")).Click();
            var dfg = team.StartProjectItemColl[1].Text;
            var hj = team.StartProjectItemColl[2].Text;
            _driver.Navigate().GoToUrl(_targetUrl);
            Assert.AreEqual(dfg, prName, "");
            Assert.AreEqual(hj, taskName);

            _driver.FindElement(By.LinkText("Settings")).Click();
            var timeSet = set.HourDisplaySet();
            _driver.Navigate().GoToUrl(_targetUrl);
            char input = (dash.StartTaskColl[2].Text)[1];

            if (timeSet == "Hours and minutes (3:45)")
            {
                var a = ':';
                Assert.AreEqual(a, input, "Time doesn't displayed according to the format on Settings page.");
            }
            else
            {
                var a = '.';
                Assert.AreEqual(a, input, "Time doesn't displayed according to the format on Settings page.");
            }
            dash.stopButton.Click();
        }

        /// <summary>
        /// Check time display by Hours and Minutes. Administration Settings 
        /// Change 'Hour display' format on Settings page
        /// 'Hours' column should display in new format
        /// </summary>
        [Test, Description("HourDisplay"), Category("Release")]
        public void _04_HourDisplay()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var admin = new AdminSettings(_driver);

            admin.TimeSetSetup(1);
            IEWait(4000);
            var e = dash.TotalTimeSum.Count;
            var text = dash.TotalTimeSum[e-1].Text;
            Assert.AreEqual(4, text.Length, "Time doesn't displayed according to the format on Settings page");

            admin.TimeSetSetup(0);
            IEWait(4000);
            var text2 = dash.TotalTimeSum[e-1].Text;
            Assert.AreEqual(7, text2.Length, "Time doesn't displayed according to the format on Settings page");
        }

        /// <summary>
        /// "A row should be added to the TimeSheet
        /// Project and Task on sheet should match those selected
        /// The column for today should show the time spent until Stop was clicked"
        /// The existing row should have an extra minute added to it for today
        /// </summary>
        [Test, Description("AddTask"), Category("Release")]
        public void _05_AddTask()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var set = new AdminSettings(_driver);

            var drop1 = dash.Rnd(dash.DropList(dash.projectDrop01).Options.Count);
            var prName = dash.FromDropProName(drop1);
            dash.DropList(dash.projectDrop01).SelectByIndex(drop1);
            IEWait(5000);
            var drop2 = dash.Rnd(dash.DropList(dash.projectDrop02).Options.Count);
            var taskName = dash.FromDropTaskName(drop2);
            dash.DropList(dash.projectDrop02).SelectByIndex(drop2);
            IEWait(6000);
            //dash.StartRndTask(drop1, drop2);
            dash.startButton.Click();
            var waitTime = 60000;
            Thread.Sleep(waitTime);
            dash.stopButton.Click();
            IEWait(6000);
            var num = dash.NewPrTaskPresent(prName, taskName);
            var pro = dash.DropList((dash.OpenProject)[num]).SelectedOption.Text;
            var task = dash.DropList((dash.OpenTasks)[num]).SelectedOption.Text;
            Assert.AreEqual(prName, pro, "project name does not match the selected.");
            Assert.AreEqual(taskName, task, "task name does not match the selected.");

            DateTime curDate = DateTime.Today.Date;
            var curDW = (int)curDate.DayOfWeek;
            //*[@id='WPQ2Table']/tbody/tr[6]/td[position()>2]/input
            var path = "(//*[@id='WPQ2Table']/tbody/tr[" + (num + 3) + "]/td[position()>2]/input)[" + curDW + "]";
           
         #region Control time set
            //_driver.FindElement(By.LinkText("Settings")).Click();
            //var timeSet = set.HourDisplaySet();
            //var a = ' ';
            //if (timeSet == "Hours and minutes (3:45)")
            //{
            //    a = ':';
            //}
            //else
            //{
            //    a = '.';
            //}
         #endregion
            var a = set.TimeSetCheck();
            _driver.Navigate().GoToUrl(_targetUrl);
            
         #region Set modul according to time set
            var mod = 0;
            if (a == ':')
            {
                mod = 60;
            }
            else
            {
                mod = 100;
            }
          #endregion

            //var TmSpent = _driver.FindElement(By.XPath(path)).GetAttribute("value");
            //string[] input = TmSpent.Split(new char[] { a });
            //var TimeSpent = (Convert.ToInt32(input[0]) * mod) + Convert.ToInt32(input[1]);

            var TimeSpent = dash.GetTimeFromeCell(path, a, mod);

            if (a == ':')
            {
                Assert.AreEqual(waitTime / waitTime, TimeSpent, "time specified in the cell does not match the time-out.");
            }
            else
            {
                Assert.AreEqual(waitTime / (waitTime / 2), TimeSpent, "time specified in the cell does not match the time-out.");
            }
            IEWait(4000);
            dash.updateButton.Click();
            IEWait(4000);
            dash.DropList(dash.projectDrop01).SelectByIndex(drop1);
            IEWait(4000);
            dash.DropList(dash.projectDrop02).SelectByIndex(drop2);
            IEWait(4000);
            dash.startButton.Click();
            Thread.Sleep(waitTime);
            IEWait(3000);
            dash.stopButton.Click();
            IEWait(3000);
            dash.updateButton.Click();

            //var TmSpent02 = _driver.FindElement(By.XPath(path)).GetAttribute("value");
            //string[] input02 = TmSpent02.Split(new char[] { a });
            //var TimeSpent02 = (Convert.ToInt32(input02[0]) * mod) + Convert.ToInt32(input02[1]);
            //IEWait(2000);
            var TimeSpent02 = dash.GetTimeFromeCell(path, a, mod);
            IEWait(3000);
            if (a == ':')
            {
                Assert.AreEqual((waitTime / waitTime) * 2, TimeSpent02, "additional time for the selected task in cell does not match the time-out.");
            }
            else
            {
                Assert.AreEqual((waitTime / (waitTime / 2)) * 2, TimeSpent02, "additional time for the selected task in cell does not match the time-out.");
            }
        }

        /// <summary>
        /// Click 'Add row'
        /// A new blank row is added immediately above the 'Add row' link
        /// </summary>
        [Test, Description("Add row"), Category("Release")]
        public void _06_AddRow()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var countPr01 = dash.OpenProject.Count;
            dash.addRowButton.Click();
            IEWait(6000);
            var countPr02 = dash.OpenProject.Count;
            Assert.AreEqual(countPr01, (countPr02 - 1), "Row was not added.");
            IEWait(6000);
            var dProName = dash.DropList(dash.projectDrop01).SelectedOption.Text;
            var dTaskName = dash.DropList(dash.projectDrop02).SelectedOption.Text;
            IEWait(6000);
            var AddProName = dash.DropList(dash.OpenProject[countPr02-1]).SelectedOption.Text;
            var AddTaskName = dash.DropList(dash.OpenTasks[countPr02-1]).SelectedOption.Text;

            Assert.AreEqual(dProName, AddProName, "New row Project Name not match to default Project Name.");
            Assert.AreEqual(dTaskName, AddTaskName, "New row Task Name not match to default Task Name.");
        }

        /// <summary>
        /// Add two rows with the same Project and Task, click Update
        /// Duplicate row validation warning
        /// 
        /// Add two rows with the same Project and Task, click Approve
        /// Duplicate row validation warning
        /// </summary>
        [Test, Description("Add two same row"), Category("Release")]
        public void _07_AddTwoSameRow()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var assertText = "There are duplicate entries on the timesheet: Please change the Project, Task, or other key column on these entries";
            dash.addRowButton.Click();
            dash.addRowButton.Click();
            dash.updateButton.Click();
            IAlert alert = _driver.SwitchTo().Alert();
            var allertText = alert.Text;
            alert.Accept();
            Assert.AreEqual(allertText, assertText, "Warning text is missing or does not match the standard.");

            dash.approveButton.Click();
            IAlert alertAp = _driver.SwitchTo().Alert();
            var allertText02 = alertAp.Text;
            alertAp.Accept();
            Assert.AreEqual(allertText02, assertText, "Warning text is missing or does not match the standard.");
        }

        /// <summary>
        /// Change one of the time values
        /// The day and week totals should update on the 'Actual total' row
        /// </summary>
        [Test, Description("Time Input Check"), Category("Release")]
        public void _08_TimeInputCheck()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var set = new AdminSettings(_driver);

            set.TimeSetSetup(0); // Setup time ':'

            dash.AddRowRndProjectAndTask();

            var countPr = dash.OpenProject.Count;
            var mod = 60;
            var cellPath = "//tbody/tr[" + (countPr + 2) + "]/td[position()>2]/input";
            IList<IWebElement> TimeCellsColl = _driver.FindElements(By.XPath(cellPath));
            
            var TotalTime = dash.TimeTextToInt(dash.TotalTimeSum[11].Text, mod);

            for (int i = 0; i < 6; i++)
            {
                

                var v = dash.TotalTimeSum[i + 1].Text;
                var TimeSpentDayBefore = dash.TimeTextToInt(v, mod);

                TimeCellsColl[i].Clear();
                TimeCellsColl[i].SendKeys("1:00");
                TimeCellsColl[i + 1].Clear();
                
                var b = dash.TotalTimeSum[i + 1].Text;
                var TimeSpentDay = dash.TimeTextToInt(b, mod);

                var a = TimeCellsColl[i].GetAttribute("value");
                var TimeSpentCell = dash.TimeTextToInt(a, mod);
                var asText = "Incorrect day time sum (in " + i + ") - day";
                Assert.AreEqual(TimeSpentDay, TimeSpentDayBefore + mod, asText);
            }

            var TotalTimeAfter = dash.TimeTextToInt(dash.TotalTimeSum[11].Text, mod);
            Assert.AreEqual(TotalTimeAfter, TotalTime + mod*6, "Incorrect total sum");
        }


        /// <summary>
        /// Click the delete icon, and confirm
        /// The row should be removed
        /// </summary>
        [Test, Description("Remove row"), Category("Release")]
        public void _09_RemoveRow()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var countPr01 = dash.OpenProject.Count;
            dash.AddRowRndProjectAndTask();
            var countPr02 = dash.OpenProject.Count;
            dash.RemoveRowImg[countPr02-1].Click();
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            var countPr03 = dash.OpenProject.Count;
            Assert.AreEqual(countPr01, countPr03, "Row was not removed");
        }

        /// <summary>
        /// Add some values to the timesheet and click Update
        /// Timesheet values are saved
        /// 'Updated' message shows next to Update button
        /// </summary>
        [Test, Description("Update added value"), Category("Release")]
        public void _10_UpdateAdded()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);

            var set = new AdminSettings(_driver);
            // Setup time ':'
            set.TimeSetSetup(0);

            var timeCells = dash.ProTimeCells;
            timeCells[7].Clear();
            var sendTime = "1:00";
            timeCells[7].SendKeys(sendTime);
            dash.updateButton.Click();
            var dfg = _driver.FindElement(By.CssSelector("span[id='WPQ2Updated']")).Displayed;
            Assert.IsTrue(dfg, "Text Updated is not displayed.");
            var timeCellsUpd = dash.ProTimeCells;
            var timeInCell = timeCellsUpd[7].GetAttribute("value");
            Assert.AreEqual(sendTime, timeInCell, "Time in a cell does not match the set time.");
        }



        /// <summary>
        /// Click left date navigation arrow a few times
        /// Date decreases by one week for each click, showing that week's timesheet
        /// 
        /// Click right date navigation arrow (>) a few times
        /// Date increases by one week for each week, showing that week's timesheet
        /// </summary>
        [Test, Description("Navigation arrow"), Category("Release")] 
        public void _11_NavigationArrow()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            DateTime curDate = DateTime.Today.Date;
            var curDW = (int)curDate.DayOfWeek;
            var dateSet = curDate.AddDays(-(curDW - 1));
            var curDateInField = dash.dateField.GetAttribute("value");
            var assDate = dash.DateString(dateSet);
            Assert.AreEqual(curDateInField, assDate, "Date is not in range.");            

            dash.previousWeek.Click();
            IEWait(4000);
            var curDateInField02 = dash.dateField.GetAttribute("value");
            var dateSetMinus = dateSet.AddDays(-7);
            var assDateMinus = dash.DateString(dateSetMinus);
            Assert.AreEqual(curDateInField02, assDateMinus, "Date is not in range.");

            _driver.Navigate().GoToUrl(_targetUrl);
            dash.nextWeek.Click();
            IEWait(4000);
            var curDateInField03 = dash.dateField.GetAttribute("value");
            var dateSetPlus = dateSet.AddDays(7);
            var assDatePlus = dash.DateString(dateSetPlus);
            Assert.AreEqual(curDateInField03, assDatePlus, "Date is not in range.");
        }

        /// <summary>
        /// Select a date at random from the date picker
        /// The date should change to the beginning of the week clicked, showing that week's timesheet
        /// </summary>
        [Test, Description("Date Picker"), Category("Release")]
        public void _12_DatePicker()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var curDateInField = dash.dateField.GetAttribute("value");
            DateTime curDate = DateTime.Today.Date;
            var dateSet = curDate.AddDays(-7);

            string[] dateSetString = dateSet.ToString().Split(new char[] { '.', ' ', ':' });
            string dateId = dateSetString[2] + dateSetString[1] + dateSetString[0];
            //string path = "//*[@id='" + dateId + "']";
            var curDW = (int)curDate.DayOfWeek;
            var dateSetMon = dateSet.AddDays(-(curDW - 1));

            dash.DatePicker.Click();
            IEWait(5000);
            _wait.Until(d => d.FindElement(By.CssSelector("iframe[id$='DateDatePickerFrame']")).Enabled); // 
            _driver.SwitchTo().Frame(_driver.FindElement(By.CssSelector("iframe[id$='DateDatePickerFrame']")));
            var ghj = _driver.PageSource; // 
            _wait.Until(d => d.FindElement(By.Id(dateId)).Enabled);
            _driver.FindElement(By.Id(dateId)).Click();
            _driver.SwitchTo().DefaultContent();
            Thread.Sleep(2000);
            IEWait(5000);
            var curDateInField02 = dash.dateField.GetAttribute("value");
                        
            Assert.AreEqual(dash.DateString(dateSetMon), curDateInField02, "Date is not in range.");
        }

        //Cannot approve: This TimeSheet contains no entries
        /// <summary>
        /// Click the Approve button on an empty timesheet
        /// Empty timesheet validation warning
        /// </summary>
        [Test, Description("Approve Empty"), Category("Release")]
        public void _13_ApproveEmpty()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            dash.nextWeek.Click();
            IEWait(4000);
            dash.approveButton.Click();
            IEWait(4000);
            var errText = "Cannot approve: This TimeSheet contains no entries";
            Assert.AreEqual(errText, dash.ErrorMess.Text, "The error message does not appear or does not match the pattern.");
        }

        /// <summary>
        /// Select a different user
        /// Timesheet for that user should be shown
        /// </summary>
        [Test, Description("Select a different user"), Category("Release")]
        public void _14_SelectDifferentUser()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            //var originalWindow = _driver.CurrentWindowHandle;
            var beforePopup = _driver.WindowHandles;
            var cells01 = dash.ProTimeCells;

            dash.ChoosePerson.Click();
            IEWait(5000);
            var allWindows = _driver.WindowHandles;
            var afterPopup = _driver.WindowHandles;
            //foreach (String option in allWindows)
            //{
            //    if (option != originalWindow)
            //    {
            //        var popUpWin = option;
            //    }
            //}


            _driver.SwitchTo().Window(afterPopup[1]); // 
            var popSelectUser = new SelectPeoplePopUp(_driver);
            IEWait(6000);
            popSelectUser.QueryTextBox.SendKeys("test");
            Thread.Sleep(2000); //  Chrome
            popSelectUser.QueryButton.Click();
            var UserColl = popSelectUser.UserColl;
            var newUser = UserColl[12].Text;
            UserColl[12].Click();
            popSelectUser.OkButton.Click();
            _driver.SwitchTo().Window(allWindows[0]);
            Thread.Sleep(2000); //  Chrome
            IEWait(4000);
            var selectUser = dash.CurrentUser.Text;
            var cells02 = dash.ProTimeCells;
            Assert.AreEqual(selectUser, newUser, "User name does not match the selected.");
            Assert.AreNotSame(cells01, cells02, "The time spent data for different users the same.");
        }

        /// <summary>
        /// Ensure there is data in the timesheet and click Approve
        /// Timesheet should become read-only
        /// Data should show in Week and Month analysis
        /// </summary>
        [Test, Description("Data Approve"), Category("Release")]
        public void _15_DataApprove()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var set = new AdminSettings(_driver);
            set.TimeSetSetup(0);    // Setup time ':'
            IEWait(5000);
            var timeCells = dash.ProTimeCells;
            timeCells[7].Clear();
            var sendTime = "1:00";
            timeCells[7].SendKeys(sendTime);
            dash.approveButton.Click();
            IEWait(8000);
            var ReadOnlyColl = _driver.FindElements(By.CssSelector(".plts_label_timesheet_readonly")); // 11 unit per row
            //var unApprove = _driver.FindElement(By.CssSelector("input[title='Unapprove']"));
            Assert.IsTrue(dash.Unapprove.Enabled, "There is no button 'Unapprove'");
            Assert.AreEqual("span", ReadOnlyColl[13].TagName, "Time in a cell does not match 'read-only'.");
            IEWait(6000);
            //new WebDriverWait(_driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.CssSelector("input[title='Unapprove']"))).Click();
            dash.Unapprove.Click();
            IEWait(5000);
            var timeCells01 = dash.ProTimeCells;
            IEWait(4000);
            var fgh = timeCells01[7].TagName;
            Assert.AreEqual("input", fgh, "Time in a cell does not match 'editable'.");
        }

        /// <summary>
        /// Add a blank row to an editable timesheet and click Update
        /// Timesheet is updated
        /// Empty row is removed
        /// </summary>
        [Test, Description("Add a blank row"), Category("Release")]
        public void _16_AddBlankRow()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var OpenProjectCount = dash.OpenProject.Count;
            dash.AddRowRndProjectAndTask();

            dash.updateButton.Click();
            IEWait(4000);
            var OpenProjectCountUp = dash.OpenProject.Count;

            Assert.AreEqual(OpenProjectCount, OpenProjectCountUp, "Empty row isn`t removed");
        }


        /// <summary>
        /// Approve this week's timesheet
        /// Attempt to punch-in by clicking 'Start'
        /// Warning message shown
        /// </summary>
        [Test, Description("Approve and clicking 'Start'"), Category("Release")]
        public void _17_ApproveStart()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            dash.approveButton.Click();
            IEWait(6000);
            dash.startButton.Click();
            IEWait(6000);
            Assert.IsTrue(dash.startErrorMessage.Displayed, "There is no warning message.");
            Assert.AreEqual(dash.startErrorMessage.Text, "Your timesheet has already been approved", "The text of the warning message does not match the pattern.");
            dash.Unapprove.Click();
            IEWait(4000);
            dash.startButton.Click();
            IEWait(4000);
            dash.approveButton.Click();
            IEWait(4000);
            Assert.IsTrue(dash.ErrorMess.Displayed, "There is no warning message.");
            Assert.AreEqual(dash.ErrorMess.Text, "Cannot approve: User is still logging time", "The text of the warning message does not match the pattern.");
            
            dash.stopButton.Click();
        }

        /// <summary>
        /// Click "Copy last week"
        /// Previous week is copied to currently displayed week
        /// etc
        /// </summary>
        [Test, Description("Approve and clicking 'Start'"), Category("Release")]
        public void _18_CopyLastWeekClick()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);
            var cells01 = dash.ProTimeCells;
            var list1 = dash.DateListInCell(cells01);

            var set = new AdminSettings(_driver);
            set.ShowCopyLastWeekBut("true");
            IEWait(5000);
            dash.nextWeek.Click();
            dash.DelAllRows();
            IEWait(5000);
            dash.CopyLastWeekLink.Click();
            var cells02 = dash.ProTimeCells;
            var list2 = dash.DateListInCell(cells02);

            Assert.AreEqual(list1, list2, "Data this week differ from the previous week.");

            dash.DelAllRows();

            dash.nextWeek.Click();

            var tyu = WinHelper.IsElementPresent(_driver, dash.CopyLastWeekLink);
            Assert.IsTrue(tyu);
            var fgh = dash.CopyLastWeekLink.GetAttribute("style").ToLower();
            string[] styleString = fgh.ToString().Split(new char[] { ';' });

            var ghj = dash.AddCopyButtColl[1].GetAttribute("src");
            string[] scrString = ghj.ToString().Split(new char[] { '/' });

            Assert.AreEqual("color: gray", styleString[0], "Link text isn`t Gray");
            Assert.AreEqual("copy_disabled.gif", scrString[scrString.Length-1]);

            dash.CopyLastWeekLink.Click();
            IAlert alert2 = _driver.SwitchTo().Alert();
            var alertText = alert2.Text;
            alert2.Accept();
            Assert.AreEqual("There are no entries on last week's timesheet", alertText, "'Alert Text' is missing or does not match the pattern.");

            set.ShowCopyLastWeekBut("false");
            dash.nextWeek.Click();
            var hjk = set.IsElementPresent(dash.CopyLastWeekLink);
            Assert.IsFalse(hjk);
            set.ShowCopyLastWeekBut("true");
        }


        /// <summary>
        /// Add a new row containing data to a timesheet, and double-click Update
        /// Only a single item is saved (no duplicates)
        /// </summary>
        [Test, Description("double-click Update"), Category("Release")]
        public void _19_DoubleClickUpdate()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();

            var dash = new DashboardPage(_driver);

            var coll1 = dash.ProTimeCells;

            dash.AddRowRndProjectAndTask();

            var coll2 = dash.ProTimeCells;
            for (int i = 0; i < 7; i++ )
            {
                coll2[coll2.Count - 7 + i].Clear();
                coll2[coll2.Count - 7 + i].SendKeys("1:00");
            }
            //dash.updateButton.Click();
            //IEWait(4000);
            //dash.updateButton.Click();
            WinHelper.FindElement(_driver, dash.updateButton, 5).Click();
            //IEWait(4000);
            //var OpenProjectCountUp = dash.OpenProject.Count;

            Assert.AreEqual(coll1.Count, coll2.Count - 7, "Number of tasks does not match the quantity.");

            var delCount = dash.DelRowColl.Count;
            //dash.DelRowColl[delCount - 1].Click();
            WinHelper.FindElement(_driver, dash.DelRowColl[delCount - 1], 7).Click();
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            dash.updateButton.Click();
        }

        /// <summary>
        /// Click 'All Site Content' on the menu
        /// No lists are displayed
        /// On the Settings page tick all three checkboxes under 'Extensibility'
        /// Under 'All Site Content' these four lists should appear: Approvals, Projects, Tasks, TimeSheets
        /// </summary>
        [Test, Description("All Site Content"), Category("Release")]
        public void _20_AllSiteContent()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            IElogin();
            var dash = new DashboardPage(_driver);
            var set = new AdminSettings(_driver);
            set.ListChBoxSet(0);
            dash.AllSiteContent.Click();
            Assert.AreEqual("All Site Content", _driver.Title);

            var allS = new AllSiteContent(_driver);
            Assert.IsTrue(allS.ApprovalsLink.Enabled);
            Assert.IsTrue(allS.ProjectsLink.Enabled);
            Assert.IsTrue(allS.TasksLink.Enabled);
            Assert.IsTrue(allS.TimesheetsLink.Enabled);
            set.ListChBoxSet(1);
            dash.AllSiteContent.Click();
            var g1 = WinHelper.IsElementPresent(_driver, allS.ApprovalsLink);
            var g2 = WinHelper.IsElementPresent(_driver, allS.ProjectsLink);
            var g3 = WinHelper.IsElementPresent(_driver, allS.TasksLink);
            var g4 = WinHelper.IsElementPresent(_driver, allS.TimesheetsLink);
            Assert.IsFalse(g1);
            Assert.IsFalse(g2);
            Assert.IsFalse(g3);
            Assert.IsFalse(g4);
            set.ListChBoxSet(0);
            
        }

        /// <summary>
        /// Click the Timesheets list
        /// Approved timesheet entries should be shown here
        /// </summary>
        [Test, Description("Click Timesheets list"), Category("Release")]
        public void _21_ClickTimesheetsList()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            IElogin();
            var dash = new DashboardPage(_driver);
            var allS = new AllSiteContent(_driver);
            dash.AllSiteContent.Click();
            allS.TimesheetsLink.Click();
            var count = _driver.FindElements(By.CssSelector(".ms-imnSpan>a")).Count;
            Assert.IsTrue(count > 0);
        }

        /// <summary>
        /// Add more than 30 approved timesheet rows
        /// Number of items on the Timesheets list remains the same
        /// </summary>
        [Test, Description("30 approved timesheet rows"), Category("Release")]
        public void _22_30_ApprovedTimesheetRows()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            IElogin();
            var dash = new DashboardPage(_driver);
            dash.DelAllRows();
            var numberOfTasks = 33;
            dash.AddManyTasks(numberOfTasks);

            dash.approveButton.Click();
            var allS = new AllSiteContent(_driver);
            dash.AllSiteContent.Click();
            allS.TimesheetsLink.Click();
            //Thread.Sleep(2000);
            //var actions = new Actions(_driver);
            //actions.MoveToElement(allS.Person).Click(allS.PersonMenu).Build().Perform();
            //Thread.Sleep(2000);

            ((IJavaScriptExecutor)_driver).ExecuteScript("var evt = document.createEvent('MouseEvents');" +
            "evt.initMouseEvent('mouseover',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
            "arguments[0].dispatchEvent(evt);", _driver.FindElement(By.CssSelector("#diidSortplts_Person")));
            //_driver.Scripts().ExecuteScript("$(#diidSortplts_Person).trigger('mouseenter').trigger('click');"); 
            Thread.Sleep(1000);

            allS.PersonMenu.Click();
            var gh = allS.UsersInMenu.Count;
            for (int i = 0; i < gh; )
            {
                if (allS.UsersInMenu[i].Text == dash.Userame)
                {
                    allS.UsersInMenu[i].Click();
                    break;
                }
                else
                {
                    i++;
                }
            }
            Assert.AreEqual(30, allS.UpprovedTasks.Count);
            allS.NextPageWithTasks.Click();
            string[] input = allS.TasksOnPage.Text.Split(new char[] { '-', ' '});
            Assert.AreEqual(numberOfTasks, Convert.ToInt32(input[3]));
            Thread.Sleep(1000);
            _driver.Navigate().GoToUrl(_targetUrl);
            dash.Unapprove.Click();
            dash.DelAllRows();
        }

    }
}


