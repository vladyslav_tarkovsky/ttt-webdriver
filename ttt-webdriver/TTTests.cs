﻿/*
using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using ttt_webdriver.PageObjects;
using System.Diagnostics;

namespace ttt_webdriver
{
    [TestFixture]
    public class TeamTimeTests
    {
        private IWebDriver _driver;
        private WebDriverWait _wait;
        private string _protocol = @"http://";
        private string _baseUrl = @"spf2010.qa.pentalogic.net:2010/TeamTime/default.aspx";
        private readonly string[,] _fullCredentials = new[,] { { "testlab2", "dobro" } };
        //Demo purposes: defaults to firefox, specify or ie, chrome. Later, this option can externalized
        private string Browser = "firefox";
        private TraceSwitch _loglevel = new TraceSwitch("General", "Entire Application");
        private string proxyaddr = "localhost";
        private int proxyport = 8080;
        public string _targetUrl = string.Empty;

        [SetUp]
        public void Setup()
        {
            switch (Browser)
            {
                case "ie":
                    _driver = new InternetExplorerDriver();
                    _targetUrl = string.Format("{0}{1}", _protocol, _baseUrl);
                    break;
                case "chrome":
                    var options = new ChromeOptions();
                    //Overriding the default browser location
                    options.BinaryLocation = "C:\\GoogleChromePortable\\GoogleChromePortable.exe";
                    options.AddArgument("start-maximized");
                    _driver = new ChromeDriver(options);
                    _targetUrl = string.Format("{0}{1}:{2}@{3}", _protocol, _fullCredentials[0, 0], _fullCredentials[0, 1], _baseUrl);
                    break;
                default:
                    var profile = new FirefoxProfile("testprofile\\");
                    _driver = new FirefoxDriver(profile);
                    _targetUrl = string.Format("{0}{1}:{2}@{3}", _protocol, _fullCredentials[0, 0], _fullCredentials[0, 1], _baseUrl);
                    break;
            }
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(20));
            _driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void Teardown()
        {
            _driver.Close();
           // _driver.Quit();
        }

        [Test, Description("HTTP Digest auth"), Category("Demo")]
        public void Authorize()
        {
            //_driver.Manage().Cookies.AddCookie(new Cookie("WSS_KeepSessionAuthenticated", "{ab8c2c7a-a655-4557-a65c-807a5e3f2a62}", "spf2010.qa.pentalogic.net", "/", new DateTime(2013, 12, 29)));
            //_driver.
            _driver.Navigate().GoToUrl(_targetUrl);
            if (Browser == "ie")
            {
                WinHelper.ProcessIeNtlmAuth("pentalogic.net\\" + _fullCredentials[0, 0], _fullCredentials[0, 1]);
                _wait.Until(d => d.FindElement(By.Id("zz15_Menu_t")).Enabled);
            }
            var dashboard = new DashboardPage(_driver);
            Assert.True(dashboard.Userame.Equals(_fullCredentials[0, 0]), "Login with {0}:{1} credentials failed", _fullCredentials[0, 0], _fullCredentials[0, 1]);
            var signedout = dashboard.Logout();
            if (Browser == "ie")
                WinHelper.CancelPopup();
            Assert.True(signedout.Text.Equals("You must close your browser to complete the sign out process."), "Logout wasn't successful");
            //Sample of logging events for any reason other than Assertion
            Trace.Write("Event");
            if (_loglevel.TraceVerbose)
                Trace.Write("verbose event");
        }

        /// <summary>
        /// Assert page title with text phraze 
        /// </summary>
        [Test, Description("Search"), Category("Demo")]
        public void Search()
        {
            _driver.Navigate().GoToUrl(_targetUrl);

            var dashboard = new DashboardPage(_driver);
            var searhText = "test"; // define a search phrase
            dashboard.SearhDash(searhText);
            Assert.AreEqual("Search Results : " + searhText, _driver.Title);
        }

        
        //[Test, Description("DelRow"), Category("Demo")]
        //public void DelRow()
        //{
        //    _driver.Navigate().GoToUrl(_targetUrl);
        //    var dashboard = new DashboardPage(_driver);
        //    dashboard.Collec();
        //}

        /// <summary>
        /// Add random task
        /// </summary>
        [Test, Description("Add_task"), Category("Demo")]
        public void Add_task()
        {
            var dashboard = new DashboardPage(_driver);
            dashboard.AddTask();
        }

        /// <summary>
        /// Delete last task
        /// </summary>
        [Test, Description("Del_task"), Category("Demo")]
        public void Del_task()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var dashboard = new DashboardPage(_driver);
            dashboard.DelTask();
        }

        /// <summary>
        /// verification of conformity "Week" and "Person" fields in Action View
        /// </summary>
        [Test, Description("TeamDashAssert"), Category("Demo")]
        public void TeamDashAssert()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var dashboard = new TeamDash(_driver);
            dashboard.TeamDashAssert();
        }

        /// <summary>
        /// Checks for messages when entering data more than 24 hours
        /// </summary>
        [Test, Description("AddWrognDataHours"), Category("Demo")]
        public void AddWrongDataHoursTest()//
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var dashboard = new DashboardPage(_driver);
            Assert.AreEqual("It is not possible to work more than 24 hours in a day.", dashboard.AddWrongDataHours());
        }


        //[Test, Description("TimeSumMon"), Category("Demo")]
        //public void TimeSumMonTest()//
        //{
        //    _driver.Navigate().GoToUrl(_targetUrl);
        //    var dashboard = new DashboardPage(_driver);
        //    dashboard.TimeSumMon();
        //}

        /// <summary>
        /// Tests for the match amount of time spent for each day. Checks the total elapsed time for all days.
        /// </summary>
        [Test, Description("TimeSum"), Category("Demo")]
        public void TimeSumTest()//
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var dashboard = new DashboardPage(_driver);
            dashboard.TimeSum();
        }

        //[Test, Description("SelectTaskAssert"), Category("Demo")]
        //public void SelectTaskAssert()//SelectTaskAssert
        //{
        //    _driver.Navigate().GoToUrl(_targetUrl);
        //    var analysis = new Analysis(_driver);
        //    analysis.SelectTaskAssert();
        //}

        /// <summary>
        /// Select random data for "View Analysis:". Checks the current date with the date in the input field. 
        /// </summary>
        [Test, Description("RndSelect"), Category("Demo")]
        public void RndSelect()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var analysis = new Analysis(_driver);
            analysis.RndSelect();
        }

        [Test, Description("PersonBrowseName"), Category("Demo")]
        public void PersonBrowseName()//SelectTaskAssert
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var analysis = new Analysis(_driver);
            analysis.PersonBrowseName();
        }

        /// <summary>
        /// Input wrong name in Analysis -> Person field. Check error text.
        /// </summary>
        [Test, Description("PersonChekWrongName"), Category("Demo")]
        public void PersonChekWrongName()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var analysis = new Analysis(_driver);
            string Name = "gggg";
            string errText = "No exact match was found for " + Name + ".";
            Assert.AreEqual(errText, analysis.PersonChekWrongName(Name));
         }

        /// <summary>
        /// Check input valid user name in Analysis -> Person field.
        /// </summary>
        [Test, Description("PersonChekValidName"), Category("Demo")]
        public void PersonChekValidName()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var analysis = new Analysis(_driver);
            string wrName = "testlab2";
            //analysis.PersonChekValidName();
            Assert.AreEqual(" " + wrName + ";", analysis.PersonChekValidName(wrName));
        }

        /// <summary>
        /// Add - Delete new project in Administration -> Projects
        /// </summary>
        [Test, Description("AddProject"), Category("Demo")]
        public void AddProject()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            admin.AddProject_();
        }

        /// <summary>
        /// New project name same as the name an existing project. Check error text.
        /// </summary>
        [Test, Description("AddProjectSameNameText"), Category("Demo")]
        public void AddProjectSameNameText()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            admin.AddProjectSameNameText();
        }

        /// <summary>
        /// Checking the functionality of the project activity checkbox.
        /// </summary>
        [Test, Description("ProActivityChek"), Category("Demo")]
        public void ProActivityChek()//SelectTaskAssert
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            admin.ProActivityChek();
        }

        /// <summary>
        /// Check list active task for project
        /// </summary>
        [Test, Description("TasksForProjectChek"), Category("Demo")]
        public void TasksForProjectChek()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            admin.TasksForProjectChek();
        }

        /// <summary>
        /// Click on task link. Check project name in Project field.
        /// </summary>
        [Test, Description("TasksClickChek"), Category("Demo")]
        public void TasksClickChek()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            admin.TasksClickChek();
        }

        /// <summary>
        ///  Add - Delete new task in Administration -> Tasks
        /// </summary>
        [Test, Description("AddTasks_"), Category("Demo")]
        public void AddTasks_()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminTasks(_driver);
            admin.AddTasks_();
        }

        /// <summary>
        /// New task name same as the name an existing task. Check error text.
        /// </summary>
        [Test, Description("AddTaskSameNameText"), Category("Demo")]
        public void AddTaskSameNameText()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminTasks(_driver);
            admin.AddTaskSameNameText();
        }

        /// <summary>
        /// Checking the functionality of the task activity checkbox.
        /// </summary>
        [Test, Description("TaskActivityChek"), Category("Demo")]
        public void TaskActivityChek()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminTasks(_driver);
            admin.TaskActivityChek();
        }

        //[Test, Description("SettingsAssert"), Category("Demo")]
        //public void SettingsAssert()//SelectTaskAssert
        //{
        //    _driver.Navigate().GoToUrl(_targetUrl);
        //    var admin = new AdminSettings(_driver);
        //    admin.SettingsAssert();
        //}

        /// <summary>
        /// Check time display by Hours and Minutes. Administration Settings 
        /// </summary>
        [Test, Description("HourDisplay01"), Category("Demo")]
        public void HourDisplay01()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            Assert.AreEqual(':', admin.HourDisplay01());
            //admin.HourDisplay01();
        }

        /// <summary>
        /// Check time display by Fractions of hours. Administration Settings
        /// </summary>
        [Test, Description("HourDisplay02"), Category("Demo")]
        public void HourDisplay02()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            Assert.AreEqual('.', admin.HourDisplay02());
            //admin.HourDisplay02();
        }

        /// <summary>
        /// Check error text about long project name
        /// </summary>
        [Test, Description("LengthLimit"), Category("Demo")]
        public void LengthLimit()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            string LengthLimit = "3";
            string NameProject = "qqqq";
            Assert.AreEqual("The Project name \"" + NameProject + "\" exceeds the maximum length (" + LengthLimit + ")", admin.LengthLimit(LengthLimit, NameProject));
            //admin.LengthLimit();
        }

        /// <summary>
        /// Checking the functionality of the "Show working hours" checkbox
        /// </summary>
        [Test, Description("ShowWorkingHours"), Category("Demo")]
        public void ShowWorkingHours()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            admin.ShowWorkingHours();
        }

        /// <summary>
        /// Checking the display assigned amount of Working hours.
        /// </summary>
        [Test, Description("DailyWorkingHours"), Category("Demo")]
        public void DailyWorkingHours()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            string DailyWorkingHours = "10";
            Assert.AreEqual(DailyWorkingHours, admin.DailyWorkingHours(DailyWorkingHours));
            //admin.DailyWorkingHours();
        }

        /// <summary>
        /// Checking the display assigned amount of Working hours per week.
        /// </summary>
        [Test, Description("WeeklyWorkingHours"), Category("Demo")]
        public void WeeklyWorkingHours()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            var WeeklyWorkingHours = "50";
            Assert.AreEqual(WeeklyWorkingHours, admin.WeeklyWorkingHours(WeeklyWorkingHours));
            //admin.WeeklyWorkingHours();
        }

        /// <summary>
        /// Checking the functionality of the "Show weekends" checkbox
        /// </summary>
        [Test, Description("ShowWeekends"), Category("Demo")]
        public void ShowWeekends()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            admin.ShowWeekends();
        }

        /// <summary>
        /// Checking the functionality of the "Copy last week" checkbox
        /// </summary>
        [Test, Description("ShowCopyLastWeekBut"), Category("Demo")]
        public void ShowCopyLastWeekBut()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminSettings(_driver);
            admin.ShowCopyLastWeekBut();
        }

        /// <summary>
        /// Check options in Project dropdown. Options should match those in Projects view.
        /// </summary>
        [Test, Description("ProjListCheck"), Category("Demo")]
        public void ProjListCheck()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var admin = new AdminProjects(_driver);
            var dash = new DashboardPage(_driver);
            Assert.AreEqual(admin.ProjListAdm(), dash.ProjList(), "Projects view options list doesn't match the dropdown contents");
        }

        /// <summary>
        /// As Project is changed, the Tasks drop down should show only the associated Tasks (as shown in the Projects view)
        /// </summary>
        [Test, Description("TasksListCheck"), Category("Demo")]
        public void TasksListCheck()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var adminPr = new AdminProjects(_driver);
            var adminTask = new AdminTasks(_driver);
            var dash = new DashboardPage(_driver);
            var link = adminPr.SelectRndActProject();
            var ListTaskForProject = adminPr.ListTaskForProject(link);
            if (ListTaskForProject[0] != "")
            {
                Assert.AreEqual(ListTaskForProject, dash.TaskList(link));
            }
            else
            {
                Assert.AreEqual(adminTask.ActiveTaskList(), dash.TaskList(link));
            }
        }

        [Test, Description("TasksListCheck"), Category("Demo")]
        public void ClickStartButton()
        {
            _driver.Navigate().GoToUrl(_targetUrl);
            var dash = new DashboardPage(_driver);
            //Select random project from the dropdown
            //Save its text value
            dash.FromDropProName(new Random().Next(0, dash.projectDrop01.Options.Count));
            //...

            //Select the random task AFTER the project was selected

            //Click start

            //Assert
            var drop1 = dash.Rnd(dash.projectDrop01.Options.Count);
            var prName = dash.FromDropProName(drop1);

            var drop2 = dash.Rnd(dash.projectDrop02.Options.Count);
            var taskName = dash.FromDropTaskName(drop2);
            dash.StartRndTask(drop1, drop2);

            var StartProName = dash.StartProName();
            var StartTaskName = dash.StartTaskName();

            Assert.IsTrue(dash.StopBottEn());
            Assert.AreEqual(dash.StartProName(), prName);
            Assert.AreEqual(dash.StartTaskName(), taskName);
        }
    }
}
*/