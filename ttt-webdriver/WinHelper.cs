﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;

namespace ttt_webdriver
{
    public static class WinHelper
    {
        

        public static bool ProcessIeNtlmAuth(string username, string password)
        {
            Thread.Sleep(100);
            SendKeys.SendWait(username);
            SendKeys.SendWait("{TAB}");
            SendKeys.SendWait(password);
            SendKeys.SendWait("{ENTER}");
            Thread.Sleep(100);
            return true;
        }

        public static void SendEnter()
        {
            SendKeys.SendWait("{ENTER}");
            Thread.Sleep(100);
        }

        public static void SendEsc()
        {
            SendKeys.SendWait("{ESC}");
            Thread.Sleep(100);
        }

        public static void CancelPopup()
        {
            SendKeys.SendWait("{TAB}");
            SendKeys.SendWait(" ");
            Thread.Sleep(100);
        }

        public static bool IsElementPresent(this IWebDriver driver, IWebElement element)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            try
            {
                if (element.Enabled)
                    return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(20));
            return false;
        }

        public static IWebElement FindElement(this IWebDriver driver, IWebElement element, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => element);
            }
            return element;
        }

        public static IJavaScriptExecutor Scripts(this IWebDriver driver)
        {
            return (IJavaScriptExecutor)driver;
        }

    }

}
